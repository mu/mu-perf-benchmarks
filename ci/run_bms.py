#!/usr/bin/env python3

import glob
import os
import sys
from os import path
from subprocess import call

# create log directory
LOG_DIR = path.join(os.getcwd(), "mubench_log")
print("Log will be put under %s" % LOG_DIR)
try:
    os.stat(LOG_DIR)
except:
    print("creating mubench_log directory...")
    os.mkdir(LOG_DIR)

def run_bm(bm):
    config_file = path.basename(bm)
    bm_name = path.splitext(config_file)[0]

    # log file (all the outputs from the execution)
    log_file = bm_name + ".log"
    log_file_path = path.join(LOG_DIR, log_file)
    f = open(log_file_path, "w")

    # result file (actual result)
    result_file = bm_name + ".result"
    result_file_path = path.join(LOG_DIR, result_file)

    print("Running mubench: %s" % bm_name)
    print("(output to: %s)" % log_file)

    cmd = [
        "python3",
        "mubench",
        "local", bm,
        "--pipeline", "",
        "--dump", result_file_path
    ]
    print(cmd)

    ret = call(cmd, stdout=f, stderr=f)

    if ret != 0:
        print("failed to execute %s: return code: %d" % (bm_name, ret))

    print()

LINE = "-------------------------------------"

print(LINE)

# get all config files
config_pattern = path.join(path.dirname(path.abspath(__file__)), "*.yml")
print("searching for all config files: " + config_pattern)
bm_configs = glob.glob(config_pattern)
bms = []
for bm_path in bm_configs:
    config_file = path.basename(bm_path)
    bm_name = path.splitext(config_file)[0]
    if bm_name.startswith("bm_"):
        bms.append(bm_name[3:])
    elif bm_name.startswith("test_"):
        bms.append(bm_name[5:])
print("all the benchmarks: %s" % bms)

# what bms we are going to run
# (if user told us specific bms to run, we run them onlythose specific bms; otherwise we run everything)
specific_bm_to_run = []
if len(sys.argv) > 1:
    specific_bm_to_run = sys.argv[1:]
else:
    specific_bm_to_run = bms
print("benchmarks to run: %s" % specific_bm_to_run)

print(LINE)

for i in range(0, len(bms)):
    bm_path = bm_configs[i]
    bm_name = bms[i]

    if bm_name in specific_bm_to_run:
        run_bm(bm_path)

print(LINE)