#!/bin/bash

LIBPFM4_DLINK="https://nchc.dl.sourceforge.net/project/perfmon2/libpfm4/libpfm-4.8.0.tar.gz"
DEST_DIR=$(pwd)/mubench/callbacks/libpfm4

if [ `uname` != 'Linux' ]; then
    echo 'Perfmon2 only supports Linux!'
    exit 1
fi

set -x  # echo and execute

wget $LIBPFM4_DLINK
tar xf libpfm-4.8.0.tar.gz
cd libpfm-4.8.0
sudo make PREFIX=$DEST_DIR install
sudo chown -R $USER:$USER $DEST_DIR
mkdir $DEST_DIR/util
cp perf_examples/perf_util.h perf_examples/perf_util.c $DEST_DIR/util
cd ..
rm libpfm-4.8.0.tar.gz
# rm -rf libpfm-4.8.0