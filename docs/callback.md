# Callback API documentation

Callbacks are used to measure the performance of benchmark tasks.

The interface of the callback is described here.
Each language has its own binding.
For each language, there can be multiple implementations of callbacks, each measuring different things, but all respect the same interface.

----
## Interface
The interface consists of 4 functions:
- `init()`
- `begin()`
- `end()`
- `report()`

The `init()` function creates a callback instance.
It accepts a parameter string `param_s`.
What and how information can be extracted is callback implementation defined.

`begin()` and `end()` mark the performance critical region. Generally, `begin()` starts a timer / counter, and `end()` stops it and read the value from it.
Each pair of these two function calls should produce a data point.
To minimise the overhead, these two functions should be kept minimal and fast.

At the end of the benchmark run, `report()` is called to dump the collected data. The data should be written to `stdout`, which will be piped to the framework and written to the result record file.

The following is an example of the interface binding in C and how it is used in a benchmark:
```c
struct Callback;    // allow for implementation defined struct

struct Callback* cb_init(const char* param_s);
void cb_begin(struct Callback* cb, void* data);    // data is implementation defined
void cb_end(struct Callback* cb, void* data);
void cb_report(struct Callback* cb);

int main(int argc, char** argv) {
    ...
    cb = cb_init(param_s);
    ...
    for(i = 0; i < iterations; i ++){
        cb_begin(cb, NULL);
        work()
        cb_end(cb, NULL);
    }
    cb_report(cb);
    return 0;
}
```

----
## Language Bindings and Benchmark Implementation
### Python
The [`__init__.py`](../mubench/suite/callbacks/python/callback/__init__.py) defines the interface.

Each implementation of the interface should inherit the `Callback` class.

`get_callback()` function is imported and called by the benchmark to dynamically select which callback implementation is to be used.
This function needs to be updated whenever a new callback implementation is to be added.
The `cb_name` parameter is the same string passed to the `name` sub-key in the `benchmark` key in the task configuration.

`get_callback()` can be imported in the benchmark implementation as follows:
```python
from callback import get_callback
```
The `PYTHONPATH` environment variable will be automatically set to include `mubench/suite/callbacks/python`.

Examples:
* callback: [cb_clock.py](../mubench/suite/callbacks/python/callback/cb_clock.py)
* benchmark: [fib.py](../mubench/suite/micro/fib/fib.py)

### RPython
Similar interface is defined for RPython as well in [`__init__.py`](../mubench/suite/callbacks/rpython/callback/__init__.py) defines the interface. Note that the interface and the implementation are in RPython.

The callback class from `get_callback()` *must* be determined at compile time, though the creation of the callback instance can be deferred until runtime (though it is possible to do it at compile time as well).
Thus the target definition should follow the following pattern as laid out in [targetfib.py](../mubench/suite/micro/fib/targetfib.py):
```python
def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]

        ...
        cb = cb_cls(cb_param)
        cb.begin()
        work()
        cb.end()
        cb.report()
        return 0

    return main, None
```

Examples:
* callback: [cb_clock.py](../mubench/suite/callbacks/rpython/callback/cb_clock.py)
* benchmark: [targetfib.py](../mubench/suite/micro/fib/targetfib.py)

### C
The [callback.h](../mubench/suite/callbacks/c/callback.h) header file defines the interface.
This file should be included in the benchmark implementation as follows:
```c
#include "callbacks.h"
```
The `-I` flag will be set at compile time to include `mubench/suite/callbacks/c` to find the relevant header files.

Each implementation of the callback interface should concretely define `struct Callback`.

Examples:
* callback: [cb_clock.c](../mubench/suite/callbacks/c/cb_clock.c)
* benchmark: [fib.c](../mubench/suite/micro/fib/fib.c)

### Mu
The callback interface is defined and enforced through the definition of relevant types, function signatures and function declarations in the C build script (see [build_callback.h](../mubench/suite/callbacks/mu/build_callbacks.h)). Note that `@mubench.t_callback` and `@mubench.t_logargs` must be defined in concrete callback implementations. They correspond to the callback type and user data type used in `@mubench.cb_begin` and `@mubench.cb_end`.


The choice of benchmark and callback implementation is done through spliting the building API calls into a few C functions:

- `mubench_build_callback()` in
[build_callback.h](../mubench/suite/callbacks/mu/build_callbacks.h)
- `mubench_build_benchmark()` in
[build_benchmark.h](../mubench/suite/callbacks/mu/build_benchmark.h)

`mubench_build_callback()` is called after the callback interface symbols are generated and defined. This callback implementation defined function should generate symbols used in the concrete implementation and define them
(see [build_clockcallback.c](../mubench/suite/callbacks/mu/build_clockcallback.c) for example).

`mubench_build_benchmark()` is called after all the callback related symbols are generated and defined. This benchmark defined function should generate and define symbols used in the specific benchmark bundle
(see [build_fib.c](../mubench/suite/micro/fib/build_fib.c) for example). This function should return the ID of the entry point function of the benchmark, which should conform to the C main function signature (`int main(int, char**)`).

The IDs for the callback interface symbols are globally accessible by including `build_callback.h`. These are included in the boot image. The global definitions in a callback implementation and a benchmark are retrieved through `mubench_get_callback_global_defs()` and `mubench_get_benchmark_global_defs()`. These two functions should allocate memory and fill the ID array `parr` with the `MuID`s of the global symbols defined, and return the length of the array. See above mentioned files for example.

If heap needs to be prebuilt in the benchmark, `mubench_build_heap()` can be defined and used for this purpose. If pointer relocation is needed, `mubench_get_reloc_info` should assign relevant values to the pointer arguments. Otherwise, these two functions should be defined as empty functions.

Each benchmark implementation should include the following headers:
```c
#include "build_benchmark.h"
```
Like C, the relevant include path will be automatically added to the C compiler by the framework.

Examples:
* callback: [build_cb_clock.c](../mubench/suite/callbacks/mu/build_cb_clock.c)
* benchmark: [build_fib.c](../mubench/suite/micro/fib/build_fib.c)

----
## Predefined Callbacks

### Clock Callback
This callback uses `clock_gettime(CLOCK_PROCESS_CPUTIME_ID, ...)` to get the amount of CPU (in user- or kernel-mode) used by the calling process to measure the performance.

This function is supported on some POSIX systems and Mac OS X 10.12+.

---
## Benchmark
The benchmarks should be placed under `mubench/suite`.
They can be grouped under sub-directories, in which case the the `name` key in the configuration should be correctly specified (e.g. `micro/fib`).
Implementations of the benchmark in different languages should be placed under the same directory of the benchmark name.

Each benchmark implementation should include code to parse the command line arguments, initialise callbacks and the measurement loop.

### Command line arguments
The order and meaning of the command line arguments passed to a benchmark are language module defined.

#### Python

| index | value                         |
| ----- | ----------------------------- |
| 0     | program name                  |
| 1     | callback class name           |
| 2     | callback parameter string     |
| 3+    | program arguments             |

#### RPython

| index | value                         |
| ----- | ----------------------------- |
| 0     | program name                  |
| 1     | callback parameter string     |
| 2+    | program arguments             |

#### C
Same as RPython

#### Mu
Same as RPython
