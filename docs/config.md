# Task Set Configuration File Format Specification

A configuration file is to be written in YAML format.

Each configuration file consists of several task sets.
The following is an example of a task set:

```yaml
fib:
  iterations: 5
  benchmark:
    name: micro/fib
    args:
      - 10  # scale factor
      - 5
  callback:
    name: clock
    param: "6"
  tasks:
    py:
      language: python
      source: fib.py
      runner:
        exec: pypy
```


-----------------------------------------------------
## Task Set Definition
A task set is defined a a top level mapping:
```yaml
taskset_name:
  iterations: # required, integer
  benchmark:  # required, mapping
  callback:   # required, string/mapping
  environ:    # optional, mapping
  outdir:     # optional, string
  recfile:     # optional, string
  tasks:      # required, mapping
    A:  # ...
    B:  # ...
    C:  # ...
```

All tasks in the task set will execute the same benchmark using the same callback.

The framework will first compile the task sources to targets.
The target name pattern is `(taskset_name)-(task_name)`.
The targets are then executed in a big loop (e.g. `A -> B -> C -> A -> ...`) over a number of `iterations`.
Each iteration should produce one data point.
The order in which the tasks are executed may not be in the same order as they are defined.

> Note: in this documentation, if the type of a key is 'string/mapping',
> then a string value will be automatically turned into a mapping with `name` subkey.
> This is convenient when no additional subkeys are to be specified.
> e.g.:
> ```yaml
> callback: clock
> ```
> is equivalent to
> ```yaml
> callback:
>   name: clock
> ```

### `benchmark`
```yaml
benchmark:
  name:   # required
  args:   # optional, list
```
`benchmark` specifies what benchmark to execute. Arguments for the benchmark can be specified in `args` subkey.
The source of the benchmark file should be specified in each task, since it is language dependent.

### `callback`
```yaml
callback:
  name: # required
  param: # optional, string
```
`callback` specifies which [callback](callback.md) is used for within-program measurements.
Only the 'core part' of the callback is to be specified in `name`.
The naming convention for callback implementation is as follows:

| language | callback implementation file |
| -------- | ---------------------------- |
| Python & RPython | `cb_<name>.py`       |
| C        | `cb_<name>.c`                |
| Mu       | `build_cb_<name>.c`          |

`param` is the parameter string passed to the callback initialisation function.
An empty string is passed by default.

### `environ`
Environment variables defined here will be visible from each task.
Each task can overwrite the task set environment variables by redefining it in its own `environ` subkey.

Below are some predefined environment variables:

| name | value |
| ---- | ----- |
| `$MUBENCH_TASKSET_NAME` | The taskset name |
| `$MUBENCH_TASK_NAME` | The task name, this will be different for each task |

Note that the system-wide environment variables are visible in the framework.
This is especially useful for path related variables.

In addition, project-wise local environment variables can be defined in the `ENVIRON` variable in `settings.py` under the project root directory (they can be overwritten by task set `environ`):
```python
ENVIRON = {
  "MU_ZEBU": "/path/to/mu-impl-fast",
  "PYPY": "/path/to/mu-client-pypy",
  "MU_HOLSTEIN": "/path/to/mu-impl-re2",
  "RPYSOM": "/path/to/RPYSOM",
  "ZEBU_BUILD": "release",
  ...
}
```

The definitions in `environ` can also contain other environment variables,
provided that they are defined in the *upper* level.
The order that the environment variables are defined is as follows:
system-wide, local `settings.py`, task set `environ`, task `environ`.
Predefined variables mentioned above are defined *prior* to this resolution.
Thus an environment variable, `PYPY_USESSION_BASENAME` for example, can be defined in task `environ` with reference to `$MUBENCH_TASK_NAME`.

In general, environment variables in the following keys are expanded:
```yaml
args:
param:
flags:
vmarg:
```


### `outdir`
The directory to where all the output and temporary files are generated.
The default is the directory where the configuration file is in.

### `recfile`
File name of a generated JSON file containing execution record (task set config and result).
This can be relative or absolute. In case of relative path, the result file is placed under `outdir`.
Any environment variables in the path is resolved.

By default the file is `<outdir>/<taskset_name>.json`.


### `tasks`
The tasks.

### `compare`
Two dimensional array that represent pairs of tasks to compare.
This will affect the output of `LogOutputPipeline`.

-----------------------------------------------------
## Task Definition

```yaml
task_name:
  language:   # required, string/mapping
  source:     # required, string
  environ:    # optional, mapping
  compiler:   # optional, language dependent, mapping
  runner:     # optional, language dependent, mapping
```

### `language`
This key specifies what language the benchmark source file is written in.
The corresponding language module is then selected to compile and run the source code.

In addition to the mandatory `name` sub-key, each language may define a set of different configuration items, see [languages](#languages).

Language names must be in lower case.

### `source`
The benchmark source file. **NAME ONLY**. The file should be placed under `mubench/suite/<bench_name>/<source>`.

### `compiler`
```yaml
compiler:
  exec:   # optional, string
  flags:  # optional, list of strings
```
Settings and flags for the compiler.
Language dependent, refer to specific language descriptions.

The common keys are:

* `exec`: path/name of compiler program. If not specified, the default compiler coded in the language module is used.
* `flags`: a list of flag strings to be passed to the compiler.

### `runner`
Settings and flags for the runner.
Also language dependent.

Has the same common keys as `compiler`.

-----------------------------------------------------
## Languages

Currently the following languages are supported:
* [Python](#python)
* [RPython](#rpython) (C and Mu backends)
* [C](#c)
* [Mu](#mu) (in the form of API calls in C source)


### Python
Run benchmark under a Python interpreter.
```yaml
language: python
runner:
  exec: # optional, string
```
Only `runner` key is configured.
The Python interpreter can be specified in the `exec` subkey, `python` is used by default.

### RPython
```yaml
environ:
  PYPY:       # required
  MU_(IMPL):  # required
language:
  name: rpython
  backend: # required, string
  impl:    # required if backend is 'mu', string
compiler:
  exec:     # optional, string
  args:     # optional, list of string
  vmargs:   # optional, list of string
runner:
  flags:    # optional
```
Compile an RPython target using a specified backend and run it natively or on a Mu implementation.

`backend` key is required in the `language` configuration.
This corresponds to the flag `--backend` flag in RPython compiler.
The acceptable backends are `c` and `mu`.

If the `backend` is `mu`, `impl` key is required to specify which Mu implementation is targeted.
This corresponds to the flag `--mu-impl` flag in the RPython compiler.
Currently known implementations are `holstein` and `zebu`.
`MU_(IMPL)` (e.g. `MU_HOLSTEIN`, `MU_ZEBU`) environment variable is required to point to the cloned repository of
[holstein](https://gitlab.anu.edu.au/mu/mu-impl-ref2) or [zebu](https://gitlab.anu.edu.au/mu/mu-impl-fast).
This is required by the Mu backend of the RPython compiler,
and it can be specified in `environ` or inherited from system-wide environment.

`PYPY` environment variable is required to point to the cloned
[mu-client-pypy](https://gitlab.anu.edu.au/mu/mu-client-pypy) repository.

An additional list of flags to the RPython compiler can be specified in the `flags` key.
The default flags `-O3` and `--no-shared` are always automatically included.

`args` key can be used to pass a list of strings to the RPython target configuration function below as the `args` argument.
```python
def target(driver, args):
    # target definition
```

If `backend` is `mu`, `vmargs` key can be used to specify VM options.
The value of this key is a list of strings.
The framework will join this list together with appropriate separators according to the selected Mu implementation and pass the joint string to the VM initialisation function.

> Note, because the framework passes a list of strings to create the subprocess,
> the flag `--mu-vmargs=` to RPython displayed on the commandline omits the quotation mark (`'`).
> If the command is copied and pasted to the command line, make sure the qutation mark is added to the
> argument string.

The `exec` key can be used to specify a Python 2.7 interpreter for compiling RPython.
By default [PyPy](http://pypy.org/) is used because of its speed.

If `impl` is `holstein`, the `flags` sub-key under `runner` can be used to pass flags to `runmu.sh`.
And the default `exec` is `$MU_HOLSTEIN/tools/runmu.sh`.

### C
Compile and run a C benchmark.
```yaml
language: c
compiler:
  exec: # optional, string
```
`clang` is the default C compiler.
This can be changed in the optional `exec` key under `compiler`.
Addional compiler flags can be specified in `flags`.

### Mu
```yaml
environ:
  MU_(IMPL):  # required, string
language:
  name: mu
  impl: # required, string
source: build_(benchname).c # required, string
compiler:
  exec: # optional, string
  vmargs: # optional, string
```
Compile and run a C source file consisting of Mu API calls to construct the Mu bundle,
then run the bundle on the specified Mu implementation.

Generally the `source` file should be in the form of `build_(bench).c`, since the C souce file is actually building the benchmark using API calls.

`impl` key is required under `language` to select a Mu implementation.

`clang` is the default C compiler. This can be changed in the optional `exec` key.

The meaning and requirement for `vmargs` key is the same with RPython.

Same requirements for `environ` and `runner` from [RPython](#rpython) apply.

