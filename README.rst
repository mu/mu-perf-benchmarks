Mu Speed Centre
===============

Framework and benchmarks for measureing performance of Mu.

Getting Started
---------------

Prerequisites
~~~~~~~~~~~~~

The framework has only been tested on Mac OS X and Linux.

Python packages
^^^^^^^^^^^^^^^

Python 3.4+ is needed to run the framework.

You can install dependencies using ``pip3 install -r requirements.txt``.

Repository Dependencies
^^^^^^^^^^^^^^^^^^^^^^^

If running RPython or Mu related tasks, the following repositories need
to be cloned:

-  `mu-client-pypy <https://gitlab.anu.edu.au/mu/mu-client-pypy>`__
-  `mu-impl-ref2 <https://gitlab.anu.edu.au/mu/mu-impl-ref2>`__
-  `mu-impl-fast <https://gitlab.anu.edu.au/mu/mu-impl-fast>`__

Please follow the relevant steps to set up / compile the repositories.

Note that for ``mu-client-pypy``, ``mu-rewrite`` branch is to be used,
and ``pypy.patch``, ``clang_opt_flag.patch`` needs to be applied.

Other Dependencies
^^^^^^^^^^^^^^^^^^

The default Python 2.7 interpreter used to compile RPython code is
`PyPy <http://pypy.org/>`__ for speed reasons. Thus it is recommended to
install PyPy. This choice of Python interpreter can be changed using the
``exec`` in the ``compiler`` key for RPython tasks. More details see
`RPython config options <docs/config.md#rpython>`__.

Installing
~~~~~~~~~~

The framework can be *optionally* installed as a Python application.

::

    pip install -U .

Running the tests
-----------------

To run the performance measurement locally you need to supply one or more YAML
files specifying the task sets. Exmaple YAML task set config files can
be found under ``example/``. The following command runs the
test config:

::

    python3 mubench local example/test_config.yml

Documentation
-------------

-  `Task set configuration file specification <docs/config.md>`__
-  `Callback and benchmark implementation
   documentation <docs/callback.md>`__
-  `Settings reference <docs/settings.md>`__


License
-------

This project is licensed under the Apache 2.0 License - see the
`LICENSE <LICENSE>`__ file for details
