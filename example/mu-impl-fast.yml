# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

fib:
  iterations: 20
  benchmark:
    name: micro/fib
    args:
      - 10  # scale factor
      - 35
  callback:
    name: clock
    param: "6"
  tasks:
    rpyc_O3:
      language:
        name: rpython
        backend: c
      source: targetfib.py
      compiler:
        flags:
          - --gc=none
      environ:
        PYPY_C_CLANG_OPT_FLAG: -O3
        PYPY_USESSION_BASENAME: fib_rpyc_O3
    rpyzebu:
      language:
        name: rpython
        backend: mu
        impl: zebu
      source: targetfib.py
      compiler:
        pypydir: $PYPY_MU
      environ:
        ZEBU_BUILD: release
        PYPY_USESSION_DIR: example
        PYPY_USESSION_BASENAME: fib_rpyzebu
    c_O3:
      language: c
      source: fib.c
      compiler:
        flags:
          - -O3
    mu_fast:
      language:
        name: mu
        impl: zebu
      source: build_fib.c
      compiler:
        flags:
          - -DBUILD_FIB_FAST
alloc:
  iterations: 20
  benchmark:
    name: micro/alloc
    args:
      - 2000  # scale factor
      - 10000
  callback:
    name: clock
    param: "6"
  tasks:
    rpyc:
      language:
        name: rpython
        backend: c
      source: targetalloc.py
      compiler:
        flags:
          - --gc=none   # disable GC on PyPy
    rpyzebu:
      language:
        name: rpython
        backend: mu
        impl: zebu
      source: targetalloc.py
      compiler:
        vmargs:
          - --gc-immixspace-size=524288000  # 500M
          - --gc-lospace-size=104587600     # 100M
except:
  benchmark:
    name: micro/except
    args:
      - 2000
      - 1
      - 500
  iterations: 20
  callback:
    name: clock
    param: "6"
  environ:
    PYPY_USESSION_DIR: example
  tasks:
    rpyc_O3:
      language:
        name: rpython
        backend: c
      source: targetexcept.py
      environ:
        PYPY_C_CLANG_OPT_FLAG: -O3
        PYPY_USESSION_BASENAME: "${MUBENCH_TASKSET_NAME}_${MUBENCH_TASK_NAME}"
    rpyzebu:
      language:
        name: rpython
        backend: mu
        impl: zebu
      source: targetexcept.py
      environ:
        PYPY_USESSION_BASENAME: except_rpyzebu
som:
  iterations: 20
  benchmark:
    name: SOM
    args:
      - 200  # scale factor
      - -cp
      - $RPYSOM/Smalltalk
      - $RPYSOM/TestSuite/TestHarness.som
  callback:
    name: clock
    param: "6"
  environ:
    PYPY_USESSION_DIR: example  # controls where PyPy puts usession directory
    PYTHONPATH: $RPYSOM/src
  tasks:
    rpyc_O3:
      language:
        name: rpython
        backend: c
      source: targetrpysom.py
      environ:
        PYPY_C_CLANG_OPT_FLAG: -O3
        PYPY_USESSION_BASENAME: som_rpyc_O3
    rpyzebu:
      language:
        name: rpython
        backend: mu
        impl: zebu
      source: targetrpysom.py
      environ:
        ZEBU_BUILD: "release"
        PYPY_USESSION_BASENAME: som_rpyzebu
      compiler:
        vmargs:
          - --gc-immixspace-size=1024288000
          - --gc-lospace-size=204587600
quicksort:
  benchmark:
    name: micro/quicksort
  iterations: 20
  callback:
    name: clock
    param: "6"
  environ:
    PYPY_USESSION_DIR: example
  tasks:
    rpyc_O0:
      language:
        name: rpython
        backend: c
      source: targetquicksort.py
      environ:
        PYPY_C_CLANG_OPT_FLAG: -O0
        PYPY_USESSION_BASENAME: &basename "${MUBENCH_TASKSET_NAME}_${MUBENCH_TASK_NAME}"
      flags:
        - --gc=none
      compiler:
        args:
          - &scale_factor 1000
    rpyc_O3:
      language:
        name: rpython
        backend: c
      source: targetquicksort.py
      environ:
        PYPY_C_CLANG_OPT_FLAG: -O3
        PYPY_USESSION_BASENAME: *basename
      flags:
        - --gc=none
      compiler:
        args:
          - *scale_factor
    rpyzebu:
      language:
        name: rpython
        backend: mu
        impl: zebu
      source: targetquicksort.py
      environ:
        PYPY_USESSION_BASENAME: *basename
      compiler:
        args:
          - *scale_factor
