#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import logging

import click

# Workaround
sys.path.insert(0, os.path.abspath(os.path.join(__file__, "..", "..")))

from mubench import __VERSION__
from mubench.frontend import frontends
from mubench.conf import settings

LOGGING_FORMAT_DEFAULT = "[%(levelname)s] %(asctime)s %(module)s %(message)s"

logging.basicConfig(
    format=getattr(settings, 'LOGGING_FORMAT', LOGGING_FORMAT_DEFAULT),
    level=logging.INFO)


def print_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo("Mu Performance Benchmarking Framework\n"
               "Version: {}".format(__VERSION__))
    ctx.exit()


@click.group()
@click.option('--version',
              is_flag=True,
              callback=print_version,
              expose_value=False,
              is_eager=True)
def main():
    pass


for frontend in frontends:
    main.add_command(frontend)

if __name__ == '__main__':
    main()
