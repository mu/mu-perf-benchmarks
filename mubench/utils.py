#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from string import Template
import os
import subprocess
import logging
import subprocess as subproc
import platform
from types import SimpleNamespace
from mubench.conf import settings
from pathlib import Path
import ctypes
import sys

logger = logging.getLogger(__name__)


def expandenv(s, env=os.environ):
    return Template(s).substitute(env)


def dictify(opt, key_name='name'):
    return {key_name: opt} if isinstance(opt, str) else opt


class ExecutionFailure(Exception):
    def __init__(self, cmd, exec_res):
        self.cmd = cmd
        self.exec_res = exec_res

    def dump(self, errlog_file):
        with errlog_file.open('w') as fp:
            fp.write('---------------- stdout ----------------\n')
            if self.exec_res.stdout:
                fp.write(self.exec_res.stdout)
            fp.write('\n')
            fp.write('---------------- stderr ----------------\n')
            if self.exec_res.stderr:
                fp.write(self.exec_res.stderr)

    def __str__(self):
        return "Executing '%s' failed." % \
               (' '.join(self.cmd) if isinstance(self.cmd, list) else self.cmd)


def run_in_subproc(cmd, env=None, **kwds):
    cmd = list(map(str, cmd))
    logger.info(" ".join(cmd))

    import resource
    ru0 = resource.getrusage(resource.RUSAGE_CHILDREN)
    t0 = ru0.ru_utime + ru0.ru_stime

    res = subproc.Popen(cmd, stdout=subproc.PIPE, stderr=subproc.PIPE,
                        env=env, **kwds)
    so, se = res.communicate()

    if settings.REDIRECT_SUBPROC or res.returncode != 0:
        print('---------------- stdout ----------------', file=sys.stderr)
        print(str(so, encoding='utf-8'), file=sys.stderr)
        print('---------------- stderr ----------------', file=sys.stderr)
        print(str(se, encoding='utf=8'), file=sys.stderr)

    res = SimpleNamespace(stdout=str(so, encoding='utf-8'),
                          stderr=str(se, encoding='utf=8'),
                          returncode=res.returncode)

    ru1 = resource.getrusage(resource.RUSAGE_CHILDREN)
    t1 = ru1.ru_utime + ru1.ru_stime

    if res.returncode != 0:
        raise ExecutionFailure(cmd, res)
    return res, (t1 - t0)


def add_path_to_ld_library_path(path_s, env):
    import sys
    libpath_var = 'DYLD_LIBRARY_PATH' if sys.platform == 'darwin' else 'LD_LIBRARY_PATH'
    env[libpath_var] = "%s:%s" % (path_s, env.get(libpath_var, ""))


def get_cpu_info():
    if platform.system() == "Linux":
        with open("/proc/cpuinfo") as cpuinfo:
            return cpuinfo.read()
    elif platform.system() == "Darwin":
        cmd = "sysctl -n machdep.cpu.brand_string"
        return subprocess.check_output(cmd.split()).decode("utf-8").strip()
    else:
        return "Not supported"


def get_uname():
    return subprocess.check_output(["uname", "-a"]).decode("utf-8").strip()


def get_hostname():
    return platform.node()


def get_zebu_ver():
    mudir = Path(settings.ENVIRON.get("MU_ZEBU", os.environ.get("MU_ZEBU")))
    libmu_dir = mudir / 'target' / settings.ENVIRON.get('ZEBU_BUILD',
                                                        os.environ.get(
                                                            "ZEBU_BUILD",
                                                            "release"))
    if platform.system() == "Linux":
        dll_ext = ".so"
    elif platform.system() == "Darwin":
        dll_ext = ".dylib"
    else:
        dll_ext = ".dll"
    libmu = ctypes.CDLL("{}/libmu{}".format(libmu_dir, dll_ext))
    libmu.mu_get_version.restype = ctypes.c_char_p
    return libmu.mu_get_version().decode("utf-8").strip()
