#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import click
import json

from mubench.frontend.utils import go_through_pipelines, platform_info
from mubench.models.revision import LocalRevision
from mubench.models.pipeline import pipelines

logger = logging.getLogger(__name__)


@click.command()
@click.argument('file', nargs=-1, type=click.Path(exists=True))
@click.option("--pipeline", default="box,bar",
              help="Combination of [{}], separated by comma".format(
                  ",".join(pipelines.keys())
              ))
@click.option('--skip_compile', default='none',
              help="a list of tasks to skip compilation." +
                   "Can be 'all', 'none', or a string in the form of 'taskset1:task1,task2;taskset2'. " +
                   "If only a task set's name is defined, skip compilation of all its tasks. " +
                   "Default target in the form of 'taskset-task' is assumed to be found under output directory.")
@click.option("--dump", default=None,
              help="Dump log file to path")
def local(file, pipeline, skip_compile, dump):
    if len(file) == 0:
        logger.fatal("Too few files")
        exit(1)
    logger.info("Constructing a LocalRevision")
    revision = LocalRevision(file)
    logger.info("Running tasks specified in file")
    revision.run_tasksets(skip_compile)
    if pipeline:
        go_through_pipelines(revision.tasksets,
                             pipeline_names=pipeline.split(","))
    if dump:
        logger.info("Dumping log to {}".format(dump))
        with open(dump, "w") as logfile:
            ts_logs = [ts.get_log() for ts in revision.tasksets]
            log = platform_info()
            log["ts"] = ts_logs
            json.dump(log, logfile)
