#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from mubench.models.pipeline import pipelines
from mubench.utils import get_cpu_info, get_hostname, get_uname, get_zebu_ver
from collections import defaultdict
from mubench.models.trails import Trails

logger = logging.getLogger(__name__)


def go_through_pipelines(report, pipeline_names):
    ps = [pipelines[name]() for name in pipeline_names]
    for pipeline in ps:
        report = pipeline.process(report)


def platform_info():
    return {
        "cpu": get_cpu_info(),
        "uname": get_uname(),
        "hostname": get_hostname(),
        "zebu": get_zebu_ver()
    }


def group_stats(datapoints):
    result = defaultdict(list)
    for pair in datapoints:
        for k, v in pair.items():
            result[k].append(v)
    result = {k: Trails("", v) for k, v in result.items()}
    return result


def group_stats_result(result):
    for ts in result["ts"]:
        ts["datapoints_grouped"] = {k: group_stats(v) for k, v in
                                    ts["datapoints"].items()}
        ts["metrics"] = iter(ts["datapoints"].values()).__next__()[0].keys()
