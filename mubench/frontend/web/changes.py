#!/usr/bin/env python3
# Copyright 2017 The Australian National University
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mubench.frontend.utils import group_stats_result


def extract(op):
    tasksets = op["ts"]
    result = {}
    for ts in tasksets:
        result[ts["name"]] = ts["datapoints_grouped"]
    return result


def compare_ts(op1, op2):
    result = {}
    for impl in op1:
        for metric in op1[impl]:
            if impl in op2 and metric in op2[impl]:
                if impl not in result:
                    result[impl] = {}
                result[impl][metric] = op1[impl][metric].compare_obj(
                    op2[impl][metric])
    return result


def compare(op1, op2):
    group_stats_result(op1)
    group_stats_result(op2)
    extracted = extract(op1)
    extracted1 = extract(op2)
    result = {}
    for ts_name in extracted:
        if ts_name in extracted1:
            result[ts_name] = compare_ts(extracted[ts_name],
                                         extracted1[ts_name])
    return result
