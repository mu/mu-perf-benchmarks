#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mubench.frontend.utils import group_stats_result
from collections import defaultdict
from mubench.conf import settings

def extract(op):
    tasksets = op["ts"]
    result = {}
    for ts in tasksets:
        result[ts["name"]] = ts["datapoints_grouped"]
    return result


def make_timeline(ops):
    for op in ops:
        group_stats_result(op)
    extracts = [extract(x) for x in ops]
    revisions = [settings.WEB_POI_COMMIT_FORMATTER(op["revision"]) for op in ops]
    timestamp = [op["timestamp"] for op in ops]
    results = defaultdict(dict)
    for (idx, x) in enumerate(extracts):
        for ts in x:
            for impl in x[ts]:
                for metric in x[ts][impl]:
                    keyname = "{}/{}/{}".format(ts, impl, metric)
                    results[keyname][idx] = x[ts][impl][metric].mean
    baseline = {}
    for k in results:
        v = results[k]
        collect_dict = [None] * len(extracts)
        for k1, v1 in v.items():
            collect_dict[k1] = v1
        ratio = [None] * len(extracts)
        for i in range(0, len(extracts)):
            if collect_dict[i]:
                if k not in baseline:
                    baseline[k] = collect_dict[i]
                ratio[i] = collect_dict[i] / baseline[k]
        results[k] = ratio
    return results, revisions
