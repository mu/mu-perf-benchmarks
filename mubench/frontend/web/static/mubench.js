/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

function timelineShowAll() {
    var traces = document.getElementById("linePlot").data;
    for (i = 0; i < traces.length; i++) {
        var trace = traces[i];
        trace.visible = true;
    }
    Plotly.redraw("linePlot");
}

function timelineHideAll() {
    var traces = document.getElementById("linePlot").data;
    for (i = 0; i < traces.length; i++) {
        var trace = traces[i];
        trace.visible = "legendonly";
    }
    Plotly.redraw("linePlot");
}

function timelineMuTimeOnly() {
    var traces = document.getElementById("linePlot").data;
    for (i = 0; i < traces.length; i++) {
        var trace = traces[i];
        var zebuIndex = trace.name.indexOf("zebu");
        var muIndex = trace.name.indexOf("mu");
        var isMu = muIndex !== -1 || zebuIndex !== -1;
        var isTime = trace.name.endsWith("time");
        if (isMu && isTime) {
            trace.visible = true;
        } else {
            trace.visible = "legendonly";
        }
    }
    Plotly.redraw("linePlot");
}