#!/usr/bin/env python3
# Copyright 2017 The Australian National University
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask, render_template, request, flash, redirect, url_for, \
    Blueprint
from mubench.conf import settings
from mubench.frontend.utils import group_stats_result
from mubench.frontend.web import changes, timeline
from mubench.frontend.web.storage import LocalStorage as storage
import click
import json

root = Blueprint('root', __name__,
                 template_folder='templates',
                 static_folder="static",
                 url_prefix=settings.WEB_PREFIX)


@root.route("/visualize_commit", methods=["POST"])
def visualize_commit():
    repo, host = settings.WEB_POI_REPO, settings.WEB_POI_HOST
    assert (request.form.get("commit") and
            request.form.get("commit")[:7].isalnum())
    result = storage.load(repo, host, request.form.get("commit")[:7])
    group_stats_result(result)
    return render_template("visualize.html", result=result)


@root.route("/visualize_file", methods=["POST"])
def visualize_file():
    if "logfile" not in request.files:
        flash("No log file uploaded")
        return redirect(url_for("index"))
    logfile = request.files["logfile"]
    result = json.loads(logfile.read())
    group_stats_result(result)
    return render_template("visualize.html", result=result)


@root.route("/tools")
def tools():
    return render_template("tools.html")


@root.route("/")
def index():
    repo, host = settings.WEB_POI_REPO, settings.WEB_POI_HOST
    result = storage.load_relative(repo, host, 0)
    result1 = storage.load_relative(repo, host, 1)
    result2 = storage.load_all(repo, host)
    tl, tl_revisions = timeline.make_timeline(result2)
    return render_template("index.html",
                           visualzing=result,
                           changes=changes.compare(result, result1),
                           timeline=tl,
                           timeline_revisions=tl_revisions)


@click.command()
@click.option("--host", default=settings.WEB_HOST)
@click.option("--port", default=settings.WEB_PORT, type=int)
@click.option("--debug", default=settings.WEB_DEBUG, is_flag=True)
def web(host, port, debug):
    app = Flask(__name__)
    app.config['SECRET_KEY'] = settings.WEB_SECRET
    app.register_blueprint(root)
    app.run(host=host, port=port, debug=debug)
