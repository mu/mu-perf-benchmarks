#!/usr/bin/env python3
# Copyright 2017 The Australian National University
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mubench.conf import settings
from pathlib import Path
import json
import os


def attach_info(log, stem):
    log["revision"] = stem.split("_")[0]
    log["timestamp"] = stem.split("_")[1]


class Storage:
    pass


class LocalStorage(Storage):
    STORAGE_PATH = Path(settings.WEB_RESULT_DIR)

    @classmethod
    def load(cls, repo, host, rev):
        dirname = cls.STORAGE_PATH / repo / host
        filename = dirname.glob("{}*".format(rev)).__next__()
        with filename.open() as logfile:
            l = json.load(logfile)
            attach_info(l, str(filename.stem))
            return l

    @classmethod
    def store(cls, repo, host, rev, timestamp, text_form):
        dirname = cls.STORAGE_PATH / repo / host
        filename = dirname / "{}_{}".format(rev, timestamp)
        os.makedirs(os.path.dirname(str(filename.absolute())), exist_ok=True)
        with filename.open("w") as logfile:
            logfile.write(text_form)

    @classmethod
    def load_relative(cls, repo, host, num):
        dirname = cls.STORAGE_PATH / repo / host
        fs = sorted(
            [f.stem for f in dirname.iterdir() if not f.stem.startswith(".")],
            key=lambda x: int(x.split("_")[1]))
        filename = dirname / fs[-(1 + num)]
        with filename.open() as logfile:
            l = json.load(logfile)
            attach_info(l, str(filename.stem))
            return l

    @classmethod
    def load_all(cls, repo, host):
        dirname = cls.STORAGE_PATH / repo / host
        fs = sorted(
            [f.stem for f in dirname.iterdir() if not f.stem.startswith(".")],
            key=lambda x: int(x.split("_")[1]))
        results = []
        for f in fs:
            with (dirname / f).open() as logfile:
                l = json.load(logfile)
                attach_info(l, str((dirname / f).stem))
                results.append(l)
        return results
