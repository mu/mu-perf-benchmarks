#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy
from scipy.stats import ttest_1samp
from mubench.conf import settings

CONFIDENCE_LEVEL = settings.CONFIDENCE_LEVEL

mean = numpy.mean
median = numpy.median
std = numpy.std


def significant_change(prev, current):
    t_score, p_value = ttest_1samp(current, prev)
    return p_value < (1 - CONFIDENCE_LEVEL)


def significant_increase(prev, current):
    t_score, p_value = ttest_1samp(current, prev)
    return p_value / 2 < (1 - CONFIDENCE_LEVEL) and t_score > 0


def significant_decrease(prev, current):
    t_score, p_value = ttest_1samp(current, prev)
    return p_value / 2 < (1 - CONFIDENCE_LEVEL) and t_score < 0
