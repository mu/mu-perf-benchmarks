/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <stdlib.h>
#include "build_callbacks.h"
#include "muapi.h"

void mubench_init_callback_symbols(MuIRBuilder *bldr)
{
    // generate symbols
    t_void = bldr->gen_sym(bldr, "@mubench.t_void");
    t_callback_p = bldr->gen_sym(bldr, "@mubench.t_callback_p");
    sig_cb_init = bldr->gen_sym(bldr, "@mubench.sig_cb_init");
    sig_cb_begin = bldr->gen_sym(bldr, "@mubench.sig_cb_begin");
    sig_cb_end = bldr->gen_sym(bldr, "@mubench.sig_cb_end");
    sig_cb_report = bldr->gen_sym(bldr, "@mubench.sig_cb_report");
    sig_cb_finish = bldr->gen_sym(bldr, "@mubench.sig_cb_finish");
    t_fp_cb_init = bldr->gen_sym(bldr, "@mubench.t_fp_cb_init");
    t_fp_cb_begin = bldr->gen_sym(bldr, "@mubench.t_fp_cb_begin");
    t_fp_cb_end = bldr->gen_sym(bldr, "@mubench.t_fp_cb_end");
    t_fp_cb_report = bldr->gen_sym(bldr, "@mubench.t_fp_cb_report");
    t_fp_cb_finish = bldr->gen_sym(bldr, "@mubench.t_fp_cb_finish");
    extern_cb_init = bldr->gen_sym(bldr, "@mubench.extern_cb_init");
    extern_cb_begin = bldr->gen_sym(bldr, "@mubench.extern_cb_begin");
    extern_cb_end = bldr->gen_sym(bldr, "@mubench.extern_cb_end");
    extern_cb_report = bldr->gen_sym(bldr, "@mubench.extern_cb_report");
    extern_cb_finish = bldr->gen_sym(bldr, "@mubench.extern_cb_finish");
    t_cchar = bldr->gen_sym(bldr, "@mubench.t_cchar");
    t_cchara = bldr->gen_sym(bldr, "@mubench.t_cchara");
    t_ccharp = bldr->gen_sym(bldr, "@mubench.t_ccharp");

    // define types
    bldr->new_type_void(bldr, t_void);
    bldr->new_type_uptr(bldr, t_callback_p, t_void);
    bldr->new_funcsig(bldr, sig_cb_init,
                      (MuTypeNode[1]){t_ccharp}, 1,
                      (MuTypeNode[1]){t_callback_p}, 1);
    bldr->new_funcsig(bldr, sig_cb_begin,
                      (MuTypeNode[1]){t_callback_p}, 1, NULL, 0);
    bldr->new_funcsig(bldr, sig_cb_end,
                      (MuTypeNode[1]){t_callback_p}, 1, NULL, 0);
    bldr->new_funcsig(bldr, sig_cb_report,
                      (MuTypeNode[1]){t_callback_p}, 1, NULL, 0);
    bldr->new_funcsig(bldr, sig_cb_finish,
                      (MuTypeNode[1]){t_callback_p}, 1, NULL, 0);
    bldr->new_type_ufuncptr(bldr, t_fp_cb_init, sig_cb_init);
    bldr->new_type_ufuncptr(bldr, t_fp_cb_begin, sig_cb_begin);
    bldr->new_type_ufuncptr(bldr, t_fp_cb_end, sig_cb_end);
    bldr->new_type_ufuncptr(bldr, t_fp_cb_report, sig_cb_report);
    bldr->new_type_ufuncptr(bldr, t_fp_cb_finish, sig_cb_finish);
    bldr->new_const_extern(bldr, extern_cb_init, t_fp_cb_init, "cb_init");
    bldr->new_const_extern(bldr, extern_cb_begin, t_fp_cb_begin, "cb_begin");
    bldr->new_const_extern(bldr, extern_cb_end, t_fp_cb_end, "cb_end");
    bldr->new_const_extern(bldr, extern_cb_report, t_fp_cb_report, "cb_report");
    bldr->new_const_extern(bldr, extern_cb_finish, t_fp_cb_finish, "cb_finish");
    bldr->new_type_int(bldr, t_cchar, 8);
    bldr->new_type_hybrid(bldr, t_cchara, NULL, 0, t_cchar);
    bldr->new_type_uptr(bldr, t_ccharp, t_cchara);
}