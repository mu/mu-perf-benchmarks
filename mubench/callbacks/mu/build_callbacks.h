/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
* Header file specifying the interface for callback building functions.
*/

#ifndef __MUBENCH_BUILD_CALLBACKS_H__
#define __MUBENCH_BUILD_CALLBACKS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "muapi.h"

#define IDARR(n, ...) \
    (MuID[n]) { __VA_ARGS__ }

/**
The interface consists of the following definitions:
    .type @mubench.t_void = void
    .type @mubench.t_callback_p = uptr<@mubench.t_void>
    .type @mubench.t_cchar = int<8>
    .type @mubench.t_cchara = hybrid<@mubench.t_cchar>
    .type @mubench.t_ccharp = uptr<@mubench.t_cchara>
    .funcsig @mubench.sig_cb_init = (@mubench.t_charp) -> (@mubench.t_callback_p)
    .funcsig @mubench.sig_cb_begin = (@mubench.t_callback_p) -> ()
    .funcsig @mubench.sig_cb_end = (@mubench.t_callback_p) -> ()
    .funcsig @mubench.sig_cb_report = (@mubench.t_callback_p) -> ()
    .funcsig @mubench.sig_cb_finish = (@mubench.t_callback_p) -> ()
    .type @mubench.t_fp_cb_init = ufuncptr<@mubench.sig_cb_init>
    .type @mubench.t_fp_cb_begin = ufuncptr<@mubench.sig_cb_begin>
    .type @mubench.t_fp_cb_end = ufuncptr<@mubench.sig_cb_end>
    .type @mubench.t_fp_cb_report = ufuncptr<@mubench.sig_cb_report>
    .type @mubench.t_fp_cb_finish = ufuncptr<@mubench.sig_cb_finish>
    .const @mubench.extern_cb_init <@mubench.t_fp_cb_init> = EXTERN "cb_init"
    .const @mubench.extern_cb_begin <@mubench.t_fp_cb_begin> = EXTERN "cb_begin"
    .const @mubench.extern_cb_end <@mubench.t_fp_cb_end> = EXTERN "cb_end"
    .const @mubench.extern_cb_report <@mubench.t_fp_cb_report> = EXTERN "cb_report"
    .const @mubench.extern_cb_finish <@mubench.t_fp_cb_finish> = EXTERN "cb_finish"
*/

MuID t_void;
MuID t_callback_p;
MuID sig_cb_init;
MuID sig_cb_begin;
MuID sig_cb_end;
MuID sig_cb_report;
MuID sig_cb_finish;
MuID t_fp_cb_init;
MuID t_fp_cb_begin;
MuID t_fp_cb_end;
MuID t_fp_cb_report;
MuID t_fp_cb_finish;
MuID extern_cb_init;
MuID extern_cb_begin;
MuID extern_cb_end;
MuID extern_cb_report;
MuID extern_cb_finish;
MuID t_cchar;
MuID t_cchara;
MuID t_ccharp;

/**
* Generate predefined symbols and define them.
* This functions needs to be called before mubench_build_callbacks.
*/
void mubench_init_callback_symbols(MuIRBuilder* bldr);

#ifdef __cplusplus
}
#endif
#endif