/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
* Header file specifying the interface that the
* benchmark building script should follow.
*/

#ifndef __MUBENCH_BUILD_BENCHMARK_H__
#define __MUBENCH_BUILD_BENCHMARK_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "muapi.h"
#include "build_callbacks.h"

/**
* Implementation defined function.
* Build the benchmark functions and relavent global defs.
* Return the primordial function ID.
*/
MuID mubench_build_benchmark(MuIRBuilder *b);

/**
* Implementation defined function.
* Build and initialise the heap.
* NOTE: called after MuIRBuilder.load().
*/
void mubench_build_heap(MuCtx *ctx);

/**
* Implementation defined function.
* Allocate memory to parr,
* fill it with global definitions that should be included in the boot image.
* Return the length of the array.
* NOTE: array memory should be freed by the caller when it's no long needed.
*/
int mubench_get_benchmark_global_defs(MuID **parr);

/**
* Implementation defined function.
* Get relocation information for make_boot_image function.
* Pointers are set to NULL if no relocation is needed.
* NOTE: caller has the responsibility of freeing the memory.
*/
void mubench_get_reloc_info(MuIRefValue **sym_flds,
                            MuCString **sym_strs,
                            MuArraySize *nsyms,
                            MuIRefValue **reloc_flds,
                            MuCString **reloc_strs,
                            MuArraySize *nrelocs);

#ifdef __cplusplus
}
#endif
#endif
