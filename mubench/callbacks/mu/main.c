/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
* The entry point to the bundle building program.
*/
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "muapi.h"
#include "build_callbacks.h"
#include "build_benchmark.h"

/**
* Choose start functions based on Mu implementations.
* Specify mu implementation with -DMU_IMPL_<IMPL>
* default is MU_IMPL_HOLSTEIN
*/
#if defined(MU_IMPL_HOLSTEIN)
#include "refimpl2-start.h"
#elif defined(MU_IMPL_ZEBU)
#include "mu-fastimpl.h"
#endif

int *muerrno;

int checkerr()
{
#if defined(MU_IMPL_HOLSTEIN)
    return *muerrno;
#elif defined(MU_IMPL_ZEBU)
    return 0;
#endif
}

MuVM *new_vm(const char *vmopts)
{
    MuVM *mu;
#if defined(MU_IMPL_HOLSTEIN)
    mu = mu_refimpl2_new_ex(vmopts);
    muerrno = mu->get_mu_error_ptr(mu); // get Mu error number pointer
    return mu;
#elif defined(MU_IMPL_ZEBU)
    return mu_fastimpl_new_with_opts(vmopts);
#endif
}

void close_vm(MuVM *mu)
{
#if defined(MU_IMPL_HOLSTEIN)
    return mu_refimpl2_close(mu);
#endif
}

int main(int argc, char **argv)
{
    MuVM *mu;
    MuCtx *ctx;
    MuIRBuilder *bldr;
    int ngdefs_cb, ngdefs_bench;
    MuID *gdefs_cb, *gdefs_bench, *gdefs;
    MuID prim_func_id;
    int haserror;

    MuIRefValue *sym_flds, *reloc_flds;
    MuCString *sym_strs, *reloc_strs;
    MuArraySize nsyms, nrelocs;
    MuFuncRefValue hmain;

    mu = new_vm(argv[1]);
    ctx = mu->new_context(mu);
    bldr = ctx->new_ir_builder(ctx);

    mubench_init_callback_symbols(bldr);
    prim_func_id = mubench_build_benchmark(bldr);
    bldr->load(bldr);
    hmain = ctx->handle_from_func(ctx, prim_func_id);
    mubench_build_heap(ctx);

    // get the global definitions and merge them into one array
    MuID predef_ids[] = {
        t_void,
        t_callback_p,
        sig_cb_init,
        sig_cb_begin,
        sig_cb_end,
        sig_cb_report,
        sig_cb_finish,
        t_fp_cb_init,
        t_fp_cb_begin,
        t_fp_cb_end,
        t_fp_cb_report,
        t_fp_cb_finish,
        extern_cb_init,
        extern_cb_begin,
        extern_cb_end,
        extern_cb_report,
        extern_cb_finish,
        t_cchar,
        t_cchara,
        t_ccharp
    };
    int npredefs = sizeof(predef_ids) / sizeof(MuID);

    ngdefs_bench = mubench_get_benchmark_global_defs(&gdefs_bench);
    gdefs = (MuID *)malloc((npredefs + ngdefs_bench) * sizeof(MuID));

    memcpy(gdefs, predef_ids, sizeof(predef_ids));
    memcpy(gdefs + npredefs, gdefs_bench, ngdefs_bench * sizeof(MuID));

    // get relocation info
    mubench_get_reloc_info(&sym_flds, &sym_strs, &nsyms, &reloc_flds, &reloc_strs, &nrelocs);

    ctx->make_boot_image(ctx, gdefs, (npredefs + ngdefs_bench),
                         hmain, NULL, NULL,
                         sym_flds, sym_strs, nsyms, reloc_flds, reloc_strs, nrelocs,
                         argv[2]);

    close_vm(mu);

    haserror = checkerr();

    // free memory
    free(gdefs);
    free(gdefs_bench);
    if (sym_flds)
    {
        free(sym_flds);
        free(sym_strs);
        free(reloc_flds);
        free(reloc_strs);
    }

    if (haserror)
    {
        fprintf(stderr, "Error thrown in Mu.\n");
        return 1;
    }
    return 0;
}