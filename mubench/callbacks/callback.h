/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/**
This header file defines callback interface for C.
*/


extern void* cb_init(const char* param_s);     // use void* to hide implementation details
extern void cb_begin(void* cb);    // data is implementation defined
extern void cb_end(void* cb);
extern void cb_report(void* cb);
extern void cb_finish(void* cb);