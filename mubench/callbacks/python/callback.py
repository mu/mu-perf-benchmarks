#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def get_callback(libcb_path):
    import ctypes
    import py
    import sys

    libcb = ctypes.CDLL(libcb_path)

    def external(name, args, result):
        fp = getattr(libcb, name)
        fp.argtypes = args
        fp.restype = result
        return fp

    c_cb_init = external("cb_init", [ctypes.c_char_p], ctypes.c_void_p)
    c_cb_begin = external("cb_begin", [ctypes.c_void_p], None)
    c_cb_end = external("cb_end", [ctypes.c_void_p], None)
    c_cb_report = external("cb_report", [ctypes.c_void_p], None)
    c_cb_finish = external("cb_finish", [ctypes.c_void_p], None)

    class Callback:
        def __init__(self, param_s):
            self.cb = c_cb_init(param_s)

        def begin(self):
            c_cb_begin(self.cb)

        def end(self):
            c_cb_end(self.cb)

        def report(self):
            c_cb_report(self.cb)

        def finish(self):
            c_cb_finish(self.cb)

    return Callback
