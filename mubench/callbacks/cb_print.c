/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <stdio.h>
#include <stdlib.h>

#include "callback.h"


void* cb_init(const char* param_s) {
    printf("initialised PrintCallback with: %s\n", param_s);
    return NULL;
}

void cb_begin(void* cb) {
    printf("begin measurement.\n");
}

void cb_end(void* cb) {
    printf("end measurement.\n");
}

void cb_report(void* cb) {
    printf("report.\n");
}

void cb_finish(void* cb) { }