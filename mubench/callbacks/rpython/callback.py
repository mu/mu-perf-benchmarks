#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def get_callback(libcb_path):
    from rpython.rtyper.lltypesystem import rffi
    import py
    import sys

    libcb = py.path.local(libcb_path)
    libext = '.dylib' if sys.platform == 'darwin' else '.so'
    libname = libcb.basename[3:libcb.basename.index(libext)]
    callback_dir = py.path.local(__file__).join('..', '..')
    eci = rffi.ExternalCompilationInfo(libraries=[libname],
                                       library_dirs=[libcb.dirname],
                                       includes=['callback.h'],
                                       include_dirs=[callback_dir])
    def external(name, args, result, compilation_info=eci, **kwds):
        return rffi.llexternal(name, args, result,
                               compilation_info=compilation_info,
                               _nowrapper=True, **kwds)

    c_cb_init = external("cb_init", [rffi.CCHARP], rffi.VOIDP)
    c_cb_begin = external("cb_begin", [rffi.VOIDP], rffi.lltype.Void)
    c_cb_end = external("cb_end", [rffi.VOIDP], rffi.lltype.Void)
    c_cb_report = external("cb_report", [rffi.VOIDP], rffi.lltype.Void)
    c_cb_finish = external("cb_finish", [rffi.VOIDP], rffi.lltype.Void)

    class Callback:
        def __init__(self, param_s):
            with rffi.scoped_str2charp(param_s) as buf:
                self.cb = c_cb_init(buf)

        def begin(self):
            c_cb_begin(self.cb)

        def end(self):
            c_cb_end(self.cb)

        def report(self):
            c_cb_report(self.cb)

        def finish(self):
            c_cb_finish(self.cb)

    return Callback