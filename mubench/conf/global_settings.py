#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

# Stats
CONFIDENCE_LEVEL = 0.95

# Database
MONGODB_HOST = "localhost"
MONGODB_PORT = "27017"
MONGODB_DATABASE = "mubench"
MONGODB_URI = "mongodb://{}:{}/{}".format(MONGODB_HOST,
                                          MONGODB_PORT,
                                          MONGODB_DATABASE)

# Executing
REDIRECT_SUBPROC = False
FAIL_EARLY = True

WEB_POI_REPO = "mu-impl-fast"
WEB_POI_HOST = "angus"
WEB_POI_COMMIT_FORMATTER = lambda x: x
WEB_RESULT_DIR = "../results"
WEB_SECRET = os.urandom(24)
WEB_PREFIX = ""
WEB_HOST = "localhost"
WEB_PORT = 5000
WEB_DEBUG = False

ENVIRON = {}
