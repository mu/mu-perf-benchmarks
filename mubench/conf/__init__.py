#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import importlib
from pathlib import Path
import os
from mubench.conf import global_settings


class ImproperBehavior(Exception):
    pass


class Settings:
    def __init__(self):
        for setting in dir(global_settings):
            if setting.isupper():
                self.__dict__[setting] = getattr(global_settings, setting)

        # import local settings
        module_name = os.environ.get("MUBENCH_SETTINGS_MODULE", "settings")

        # try to find local settings.py at project root
        PROJ_ROOT = Path(__file__).joinpath('..', '..', '..').resolve()
        import sys
        old_sys_path = sys.path.copy()
        sys.path.insert(0, str(PROJ_ROOT))
        try:
            mod = importlib.import_module(module_name)
            for setting in dir(mod):
                if setting.isupper():
                    self.__dict__[setting] = getattr(mod, setting)
        except ModuleNotFoundError:
            pass
        sys.path = old_sys_path

    def __setattr__(self, name, value):
        raise ImproperBehavior("Altering settings at runtime is not allowed.")


settings = Settings()
