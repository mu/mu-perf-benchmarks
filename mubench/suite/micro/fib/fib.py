#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def fib(n):
    if n in (0, 1):
        return 1
    return fib(n - 1) + fib(n - 2)


def main(argv):
    from callback import get_callback
    libcb_path, cb_param = argv[1:3]
    cb = get_callback(libcb_path)(cb_param)

    scale_factor = int(argv[3])
    n = int(argv[4])

    s = 0
    cb.begin()
    for i in range(scale_factor):
        s += fib(n)
    cb.end()
    cb.report()
    cb.finish()
    return s != fib(n) * scale_factor


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
