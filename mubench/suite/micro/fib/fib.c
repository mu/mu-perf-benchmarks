/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "callback.h"


int fib(int n) {
    if(n == 0 || n == 1)
        return 1;
    return fib(n - 1) + fib(n - 2);
}


int main(int argc, char** argv) {
    int n, scale_factor;
    char* resfile;
    void* cb;
    int sum = 0;

    cb = cb_init(argv[1]);

    scale_factor = atoi(argv[2]);
    n = atoi(argv[3]);

    cb_begin(cb);
    for(int i = n + 1; i < (scale_factor + n + 1); i ++)
        sum += fib(n % i);
    cb_end(cb);
    printf("sum = %d\n", sum);
    cb_report(cb);
    cb_finish(cb);
    return 0;
}