/*
Copyright 2017 The Australian National University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "muapi.h"
#include "build_benchmark.h"
#include "build_callbacks.h"
/**
* Builds the following bundle:
    .typedef @t_i1 = int<1>
    .typedef @t_i32 = int<32>
    .typedef @t_hccharp = hybrid<@mubench.ccharp>
    .typedef @t_ccharpp = uptr<@t_hccharp>
    .typedef @t_pccharp = uptr<@mubench.ccharp>
    .const @c_0 <@t_i32> = 0
    .const @c_1 <@t_i32> = 1
    .const @c_2 <@t_i32> = 2
    .const @c_3 <@t_i32> = 3
    .const @c_4 <@t_i32> = 4
    .funcsig @sig_fib = (@t_i32) -> (@t_i32)
    .funcsig @sig_main = (@t_i32 @mubench.ccharp) -> (@t_i32)
    .funcsig @sig_atoi = (@mubench.ccharp) -> (@t_i32)
    .funcsig @sig_exit = (@t_i32) -> ()
    .typedef @t_fp_atoi = ufuncptr<@sig_atoi>
    .typedef @t_fp_exit = ufuncptr<@sig_exit>
    .const @extern_atoi <@t_fp_atoi> = EXTERN "atoi"
    .const @extern_exit <@t_fp_exit> = EXTERN "exit"
    .funcdef @fib VERSION %1 <@sig_fib> {
        %blk0(<@t_i32> %n):
            %eq_0 = EQ <@t_i32> %n @c_0
            %eq_1 = EQ <@t_i32> %n @c_1
            %or_res = OR <@t_i1> %eq_0 %eq_1
            BRANCH2 %or_res %blk1() %blk2(%n)
        %blk1():
            RET (@c_1)
        %blk2(<@t_i32> %k):
            %k_1 = SUB <@t_i32> %k @c_1
            %k_2 = SUB <@t_i32> %k @c_2
            %fib_1 = CALL <@sig_fib> @fib (%k_1)
            %fib_2 = CALL <@sig_fib> @fib (%k_2)
            %res = ADD <@t_i32> %fib_1 %fib_2
            RET (%res)
    }
    .funcdef @entry VERSION %v1 <@sig_main> {
        %blk0(<@t_i32> %argc <@t_ccharpp> %argv):
            %argv_var = GETVARPARTIREF PTR <@t_hccharp> %argv
            %p_argv_1 = SHIFTIREF PTR <@mubench.ccharp @t_i32> %argv_var @c_1
            %argv_1 = LOAD PTR <@mubench.ccharp> %p_argv_1
            %p_argv_2 = SHIFTIREF PTR <@mubench.ccharp @t_i32> %argv_var @c_2
            %argv_2 = LOAD PTR <@mubench.ccharp> %p_argv_2
            %p_argv_3 = SHIFTIREF PTR <@mubench.ccharp @t_i32> %argv_var @c_3
            %argv_3 = LOAD PTR <@mubench.ccharp> %p_argv_3

            %cb = CCALL <@mubench.t_fp_cb_init @mubench.sig_cb_init> @mubench.extern_cb_init (%argv_1)
            %scale_factor = CCALL #DEFAULT <@t_fp_atoi @sig_atoi> @extern_atoi (%argv_2)
            %n = CCALL #DEFAULT <@t_fp_atoi @sig_atoi> @extern_atoi (%argv_3)

            CCALL <@mubench.t_fp_cb_begin @mubench.sig_cb_begin> @mubench.extern_cb_begin (%cb)

            BRANCH %blk1(@c_0 @c_0 %scale_factor %n %cb)

        %blk1(<@t_i32> %i <@t_i32> %sum <@t_i32> %scale_factor <@t_i32> %n <@mubench.t_callback_p> %cb):
            %cmp_res = SLT <@t_i32> %i %scale_factor
            BRANCH2 %cmp_res %blk2(%i %sum %scale_factor %n %cb) %blk3(%sum %n %scale_factor %cb)

        %blk2(<@t_i32> %i <@t_i32> %sum <@t_i32> %scale_factor <@t_i32> %n <@mubench.t_callback_p> %cb):
            %fib_res = CALL <@sig_fib> @fib (%n)
            %sum_inc = ADD <@t_i32> %sum %fib_res
            %i_suc = ADD <@t_i32> %i @c_1
            BRANCH %blk1(%i_suc %sum_inc %scale_factor %n %cb)

        %blk3(<@t_i32> %sum <@t_i32> %n <@t_i32> %scale_factor <@mubench.t_callback_p> %cb):
            CCALL <@mubench.t_fp_cb_end @mubench.sig_cb_end> @mubench.extern_cb_end (%cb)
            CCALL <@mubench.t_fp_cb_report @mubench.sig_cb_report> @mubench.extern_cb_report (%cb)
            CCALL <@mubench.t_fp_cb_finish @mubench.sig_cb_finish> @mubench.extern_cb_finish (%cb)
            %fib_n = CALL <@sig_fib> @fib (%n)
            %expected = MUL <@t_i32> %fib_n %scale_factor
            %cmpres = NE <@t_i32> %sum %expected
            %retcode = ZEXT <@t_i1 @t_i32> %cmpres
            CCALL #DEFAULT <@t_fp_exit @sig_exit> @extern_exit (%retcode)
            RET (@c_0)
    }
*/
static MuID t_i1, t_i32, t_hccharp, t_ccharpp, t_pccharp, t_fp_atoi, t_fp_exit;
static MuID c_0, c_1, c_2, c_3, c_4;
static MuID sig_fib, sig_main, sig_atoi, sig_exit;
static MuID extern_atoi, extern_exit;
static MuID fib, entry;

void build_func_fib(MuIRBuilder *b);
void build_func_fib_fast(MuIRBuilder *b);
void build_func_entry(MuIRBuilder *b);

MuID mubench_build_benchmark(MuIRBuilder *b)
{
    t_i1 = b->gen_sym(b, "@t_i1");
    t_i32 = b->gen_sym(b, "@t_i32");
    t_hccharp = b->gen_sym(b, "@t_hccharp");
    t_ccharpp = b->gen_sym(b, "@t_ccharpp");
    t_pccharp = b->gen_sym(b, "@t_pccharp");
    t_fp_atoi = b->gen_sym(b, "@t_fp_atoi");
    t_fp_exit = b->gen_sym(b, "@t_fp_exit");
    c_0 = b->gen_sym(b, "@c_0");
    c_1 = b->gen_sym(b, "@c_1");
    c_2 = b->gen_sym(b, "@c_2");
    c_3 = b->gen_sym(b, "@c_3");
    c_4 = b->gen_sym(b, "@c_4");
    sig_fib = b->gen_sym(b, "@sig_fib");
    sig_main = b->gen_sym(b, "@sig_main");
    sig_atoi = b->gen_sym(b, "@sig_atoi");
    sig_exit = b->gen_sym(b, "@sig_exit");
    extern_atoi = b->gen_sym(b, "@extern_atoi");
    extern_exit = b->gen_sym(b, "@extern_exit");
    fib = b->gen_sym(b, "@fib");
    entry = b->gen_sym(b, "@entry");

    b->new_type_int(b, t_i1, 1);
    b->new_type_int(b, t_i32, 32);
    b->new_type_hybrid(b, t_hccharp, NULL, 0, t_ccharp);
    b->new_type_uptr(b, t_ccharpp, t_hccharp);
    b->new_type_uptr(b, t_pccharp, t_ccharp);
    b->new_const_int(b, c_0, t_i32, 0);
    b->new_const_int(b, c_1, t_i32, 1);
    b->new_const_int(b, c_2, t_i32, 2);
    b->new_const_int(b, c_3, t_i32, 3);
    b->new_const_int(b, c_4, t_i32, 4);
    b->new_funcsig(b, sig_fib, IDARR(1, t_i32), 1, IDARR(1, t_i32), 1);
    b->new_funcsig(b, sig_main, IDARR(2, t_i32, t_ccharpp), 2, IDARR(1, t_i32), 1);
    b->new_funcsig(b, sig_atoi, IDARR(1, t_ccharp), 1, IDARR(1, t_i32), 1);
    b->new_funcsig(b, sig_exit, IDARR(1, t_i32), 1, NULL, 0);
    b->new_type_ufuncptr(b, t_fp_atoi, sig_atoi);
    b->new_type_ufuncptr(b, t_fp_exit, sig_exit);
    b->new_const_extern(b, extern_atoi, t_fp_atoi, "atoi");
    b->new_const_extern(b, extern_exit, t_fp_exit, "exit");

#ifdef BUILD_FIB_FAST
    build_func_fib_fast(b);
#else
    build_func_fib(b);
#endif
    build_func_entry(b);
    return entry;
}

void build_func_fib(MuIRBuilder *b)
{
    MuID v1, blk0, blk1, blk2;
    MuID n, eq_0, eq_1, or_res, op_eq0, op_eq1, op_or, op_br2, if_true, if_false;
    MuID op_ret_1;
    MuID k, k_1, k_2, fib_1, fib_2, res, op_sub1, op_sub2, op_call_1, op_call_2, op_add, op_ret_2;

    v1 = b->gen_sym(b, "@fib.v1");
    blk0 = b->gen_sym(b, "@fib.v1.blk0");
    blk1 = b->gen_sym(b, "@fib.v1.blk1");
    blk2 = b->gen_sym(b, "@fib.v1.blk2");

    // blk0
    n = b->gen_sym(b, "@fib.v1.blk0.n");
    eq_0 = b->gen_sym(b, "@fib.v1.blk0.eq_0");
    eq_1 = b->gen_sym(b, "@fib.v1.blk0.eq_1");
    or_res = b->gen_sym(b, "@fib.v1.blk0.or_res");
    op_eq0 = b->gen_sym(b, "@fib.v1.blk0.op_eq0");
    op_eq1 = b->gen_sym(b, "@fib.v1.blk0.op_eq1");
    op_or = b->gen_sym(b, "@fib.v1.blk0.op_or");
    if_true = b->gen_sym(b, NULL);
    if_false = b->gen_sym(b, NULL);
    op_br2 = b->gen_sym(b, "@fib.v1.blk0.op_br2");
    b->new_cmp(b, op_eq0, eq_0, MU_CMP_EQ, t_i32, n, c_0);
    b->new_cmp(b, op_eq1, eq_1, MU_CMP_EQ, t_i32, n, c_1);
    b->new_binop(b, op_or, or_res, MU_BINOP_OR, t_i1, eq_0, eq_1, MU_NO_ID);
    b->new_dest_clause(b, if_true, blk1, NULL, 0);
    b->new_dest_clause(b, if_false, blk2, IDARR(1, n), 1);
    b->new_branch2(b, op_br2, or_res, if_true, if_false);
    b->new_bb(b, blk0, IDARR(1, n), IDARR(1, t_i32), 1, MU_NO_ID,
              IDARR(4, op_eq0, op_eq1, op_or, op_br2), 4);

    // blk1
    op_ret_1 = b->gen_sym(b, "@fib.v1.blk1.op_ret_1");
    b->new_ret(b, op_ret_1, IDARR(1, c_1), 1);
    b->new_bb(b, blk1, NULL, NULL, 0, MU_NO_ID, IDARR(1, op_ret_1), 1);

    // blk2
    k = b->gen_sym(b, "@fib.v1.blk2.k");
    k_1 = b->gen_sym(b, "@fib.v1.blk2.k_1");
    k_2 = b->gen_sym(b, "@fib.v1.blk2.k_2");
    fib_1 = b->gen_sym(b, "@fib.v1.blk2.fib_1");
    fib_2 = b->gen_sym(b, "@fib.v1.blk2.fib_2");
    res = b->gen_sym(b, "@fib.v1.blk2.res");
    op_sub1 = b->gen_sym(b, "@fib.v1.blk2.op_sub1");
    op_sub2 = b->gen_sym(b, "@fib.v1.blk2.op_sub2");
    op_call_1 = b->gen_sym(b, "@fib.v1.blk2.op_call_1");
    op_call_2 = b->gen_sym(b, "@fib.v1.blk2.op_call_2");
    op_add = b->gen_sym(b, "@fib.v1.blk2.op_add");
    op_ret_2 = b->gen_sym(b, "@fib.v1.blk2.op_ret_2");
    b->new_binop(b, op_sub1, k_1, MU_BINOP_SUB, t_i32, k, c_1, MU_NO_ID);
    b->new_binop(b, op_sub2, k_2, MU_BINOP_SUB, t_i32, k, c_2, MU_NO_ID);
    b->new_call(b, op_call_1, IDARR(1, fib_1), 1, sig_fib, fib, IDARR(1, k_1), 1, MU_NO_ID, MU_NO_ID);
    b->new_call(b, op_call_2, IDARR(1, fib_2), 1, sig_fib, fib, IDARR(1, k_2), 1, MU_NO_ID, MU_NO_ID);
    b->new_binop(b, op_add, res, MU_BINOP_ADD, t_i32, fib_1, fib_2, MU_NO_ID);
    b->new_ret(b, op_ret_2, IDARR(1, res), 1);
    b->new_bb(b, blk2, IDARR(1, k), IDARR(1, t_i32), 1, MU_NO_ID,
              IDARR(6, op_sub1, op_sub2, op_call_1, op_call_2, op_add, op_ret_2), 6);

    b->new_func(b, fib, sig_fib);
    b->new_func_ver(b, v1, fib, IDARR(3, blk0, blk1, blk2), 3);
}

void build_func_fib_fast(MuIRBuilder *irbuilder)
{
#define G(id, ...) global_ ## id
#define L(id, ...)  local_ ## id

	MuID G(0, int32);
	MuID G(2);
	MuID G(4);
	MuID G(5);
	MuID G(6);

	G(0, int32) = irbuilder->gen_sym(irbuilder, "@int32");
	irbuilder->new_type_int(irbuilder, G(0, int32), 32);

	G(2) = sig_fib;
//	G(2) = irbuilder->gen_sym(irbuilder, "@__2");
//	irbuilder->new_funcsig(irbuilder, G(2), (MuTypeNode[]){G(0, int32)}, 1, (MuTypeNode[]){G(0, int32)}, 1);
	irbuilder->new_func(irbuilder, fib, G(2));
	MuID L(0, fib.__3) = irbuilder->gen_sym(irbuilder, "@fib.__3");
	MuID L(1, fib.__3.1) = irbuilder->gen_sym(irbuilder, "@fib.__3.1");
	MuID L(2, fib.__3.1.0) = irbuilder->gen_sym(irbuilder, "@fib.__3.1.0");

	MuID L(3) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(4, fib.__3.1.2) = irbuilder->gen_sym(irbuilder, "@fib.__3.1.2");
	G(4) = irbuilder->gen_sym(irbuilder, "@__4");
	irbuilder->new_const_int(irbuilder, G(4), G(0, int32), 1);
	irbuilder->new_binop(irbuilder, L(3), L(4, fib.__3.1.2), MU_BINOP_OR, G(0, int32), L(2, fib.__3.1.0), G(4), MU_NO_ID);

	MuID L(5) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(6, fib.__3.1.3) = irbuilder->gen_sym(irbuilder, "@fib.__3.1.3");
	irbuilder->new_cmp(irbuilder, L(5), L(6, fib.__3.1.3), MU_CMP_EQ, G(0, int32), L(4, fib.__3.1.2), G(4));

	MuID L(7) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(8) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(9, fib.__3.15) = irbuilder->gen_sym(irbuilder, "@fib.__3.15");
	irbuilder->new_dest_clause(irbuilder, L(8), L(9, fib.__3.15), (MuVarNode[]){G(4)}, 1);
	MuID L(10) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(12, fib.__3.5) = irbuilder->gen_sym(irbuilder, "@fib.__3.5");
	irbuilder->new_dest_clause(irbuilder, L(10), L(12, fib.__3.5), (MuVarNode[]){L(2, fib.__3.1.0), G(4)}, 2);
	irbuilder->new_branch2(irbuilder, L(7), L(6, fib.__3.1.3), L(8), L(10));
	irbuilder->new_bb(irbuilder, L(1, fib.__3.1), (MuID[]){L(2, fib.__3.1.0)}, (MuTypeNode[]){G(0, int32)}, 1, MU_NO_ID, (MuInstNode[]){L(3), L(5), L(7)}, 3);
	MuID L(13, fib.__3.5.6) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.6");
	MuID L(14, fib.__3.5.7) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.7");

	MuID L(15) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(16, fib.__3.5.8) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.8");
	G(5) = irbuilder->gen_sym(irbuilder, "@__5");
	irbuilder->new_const_int(irbuilder, G(5), G(0, int32), (uint32_t)(-1));
	irbuilder->new_binop(irbuilder, L(15), L(16, fib.__3.5.8), MU_BINOP_ADD, G(0, int32), L(13, fib.__3.5.6), G(5), MU_NO_ID);

	MuID L(17) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(18, fib.__3.5.9) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.9");
	irbuilder->new_call(irbuilder, L(17), (MuVarNode[]){L(18, fib.__3.5.9)}, 1, G(2), fib, (MuVarNode[]){L(16, fib.__3.5.8)}, 1, MU_NO_ID, MU_NO_ID);

	MuID L(19) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(20, fib.__3.5.10) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.10");
	G(6) = irbuilder->gen_sym(irbuilder, "@__6");
	irbuilder->new_const_int(irbuilder, G(6), G(0, int32), (uint32_t)(-2));
	irbuilder->new_binop(irbuilder, L(19), L(20, fib.__3.5.10), MU_BINOP_ADD, G(0, int32), L(13, fib.__3.5.6), G(6), MU_NO_ID);

	MuID L(21) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(22, fib.__3.5.11) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.11");
	irbuilder->new_binop(irbuilder, L(21), L(22, fib.__3.5.11), MU_BINOP_ADD, G(0, int32), L(18, fib.__3.5.9), L(14, fib.__3.5.7), MU_NO_ID);

	MuID L(23) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(24, fib.__3.5.12) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.12");
	irbuilder->new_binop(irbuilder, L(23), L(24, fib.__3.5.12), MU_BINOP_OR, G(0, int32), L(20, fib.__3.5.10), G(4), MU_NO_ID);

	MuID L(25) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(26, fib.__3.5.13) = irbuilder->gen_sym(irbuilder, "@fib.__3.5.13");
	irbuilder->new_cmp(irbuilder, L(25), L(26, fib.__3.5.13), MU_CMP_EQ, G(0, int32), L(24, fib.__3.5.12), G(4));

	MuID L(27) = irbuilder->gen_sym(irbuilder, NULL);
	MuID L(28) = irbuilder->gen_sym(irbuilder, NULL);
	irbuilder->new_dest_clause(irbuilder, L(28), L(9, fib.__3.15), (MuVarNode[]){L(22, fib.__3.5.11)}, 1);
	MuID L(29) = irbuilder->gen_sym(irbuilder, NULL);
	irbuilder->new_dest_clause(irbuilder, L(29), L(12, fib.__3.5), (MuVarNode[]){L(20, fib.__3.5.10), L(22, fib.__3.5.11)}, 2);
	irbuilder->new_branch2(irbuilder, L(27), L(26, fib.__3.5.13), L(28), L(29));
	irbuilder->new_bb(irbuilder, L(12, fib.__3.5), (MuID[]){L(13, fib.__3.5.6), L(14, fib.__3.5.7)}, (MuTypeNode[]){G(0, int32), G(0, int32)}, 2, MU_NO_ID, (MuInstNode[]){L(15), L(17), L(19), L(21), L(23), L(25), L(27)}, 7);
	MuID L(30, fib.__3.15.16) = irbuilder->gen_sym(irbuilder, "@fib.__3.15.16");

	MuID L(31) = irbuilder->gen_sym(irbuilder, NULL);
	irbuilder->new_ret(irbuilder, L(31), (MuVarNode[]){L(30, fib.__3.15.16)}, 1);
	irbuilder->new_bb(irbuilder, L(9, fib.__3.15), (MuID[]){L(30, fib.__3.15.16)}, (MuTypeNode[]){G(0, int32)}, 1, MU_NO_ID, (MuInstNode[]){L(31)}, 1);
	irbuilder->new_func_ver(irbuilder, L(0, fib.__3), fib, (MuBBNode[]){L(1, fib.__3.1), L(12, fib.__3.5), L(9, fib.__3.15)}, 3);
}

void build_func_entry(MuIRBuilder *b)
{
    MuID v1, blk0, blk1, blk2, blk3;

    v1 = b->gen_sym(b, "@entry.v1");
    blk0 = b->gen_sym(b, "@entry.v1.blk0");
    blk1 = b->gen_sym(b, "@entry.v1.blk1");
    blk2 = b->gen_sym(b, "@entry.v1.blk2");
    blk3 = b->gen_sym(b, "@entry.v1.blk3");

    { // blk0
        MuID argc, argv, argv_var;
        MuID p_argv_1, p_argv_2, p_argv_3;
        MuID argv_1, argv_2, argv_3;
        MuID cb, scale_factor, n;
        MuID ops[12];

        argc = b->gen_sym(b, "@entry.v1.blk0.argc");
        argv = b->gen_sym(b, "@entry.v1.blk0.argv");
        argv_var = b->gen_sym(b, "@entry.v1.blk0.argv_var");
        p_argv_1 = b->gen_sym(b, "@entry.v1.blk0.p_argv_1");
        p_argv_2 = b->gen_sym(b, "@entry.v1.blk0.p_argv_2");
        p_argv_3 = b->gen_sym(b, "@entry.v1.blk0.p_argv_3");
        argv_1 = b->gen_sym(b, "@entry.v1.blk0.argv_1");
        argv_2 = b->gen_sym(b, "@entry.v1.blk0.argv_2");
        argv_3 = b->gen_sym(b, "@entry.v1.blk0.argv_3");
        cb = b->gen_sym(b, "@entry.v1.blk0.cb");
        scale_factor = b->gen_sym(b, "@entry.v1.blk0.scale_factor");
        n = b->gen_sym(b, "@entry.v1.blk0.n");
        for (int i = 0; i < 12; i ++)
            ops[i] = b->gen_sym(b, NULL);

        b->new_getvarpartiref(b, ops[0], argv_var, true, t_hccharp, argv);
        b->new_shiftiref(b, ops[1], p_argv_1, true, t_ccharp, t_i32, argv_var, c_1);
        b->new_load(b, ops[2], argv_1, true, MU_ORD_NOT_ATOMIC, t_ccharp, p_argv_1, MU_NO_ID);
        b->new_shiftiref(b, ops[3], p_argv_2, true, t_ccharp, t_i32, argv_var, c_2);
        b->new_load(b, ops[4], argv_2, true, MU_ORD_NOT_ATOMIC, t_ccharp, p_argv_2, MU_NO_ID);
        b->new_shiftiref(b, ops[5], p_argv_3, true, t_ccharp, t_i32, argv_var, c_3);
        b->new_load(b, ops[6], argv_3, true, MU_ORD_NOT_ATOMIC, t_ccharp, p_argv_3, MU_NO_ID);

        b->new_ccall(b, ops[7], IDARR(1, cb), 1, MU_CC_DEFAULT,
                     t_fp_cb_init, sig_cb_init, extern_cb_init,
                     IDARR(1, argv_1), 1, MU_NO_ID, MU_NO_ID);
        b->new_ccall(b, ops[8], IDARR(1, scale_factor), 1, MU_CC_DEFAULT,
                     t_fp_atoi, sig_atoi, extern_atoi,
                     IDARR(1, argv_2), 1, MU_NO_ID, MU_NO_ID);
        b->new_ccall(b, ops[9], IDARR(1, n), 1, MU_CC_DEFAULT,
                     t_fp_atoi, sig_atoi, extern_atoi,
                     IDARR(1, argv_3), 1, MU_NO_ID, MU_NO_ID);

        b->new_ccall(b, ops[10], NULL, 0, MU_CC_DEFAULT,
                     t_fp_cb_begin, sig_cb_begin, extern_cb_begin,
                     IDARR(1, cb), 1, MU_NO_ID, MU_NO_ID);

        MuID dst = b->gen_sym(b, NULL);
        b->new_dest_clause(b, dst, blk1, IDARR(5, c_0, c_0, scale_factor, n, cb), 5);
        b->new_branch(b, ops[11], dst);

        b->new_bb(b, blk0, IDARR(2, argc, argv), IDARR(2, t_i32, t_ccharpp), 2, MU_NO_ID, ops, 12);
    }

    { // blk1
        MuID i, sum, scale_factor, n, cb, cmp_res;
        MuID op_SLT, if_true, if_false, op_br2;
        i = b->gen_sym(b, "@entry.v1.blk1.i");
        sum = b->gen_sym(b, "@entry.v1.blk1.sum");
        scale_factor = b->gen_sym(b, "@entry.v1.blk1.scale_factor");
        n = b->gen_sym(b, "@entry.v1.blk1.n");
        cb = b->gen_sym(b, "@entry.v1.blk1.cb");
        cmp_res = b->gen_sym(b, "@entry.v1.blk1.cmp_res");
        op_SLT = b->gen_sym(b, "@entry.v1.blk1.op_SLT");
        if_true = b->gen_sym(b, "@entry.v1.blk1.if_true");
        if_false = b->gen_sym(b, "@entry.v1.blk1.if_false");
        op_br2 = b->gen_sym(b, "@entry.v1.blk1.op_br2");
        b->new_cmp(b, op_SLT, cmp_res, MU_CMP_SLT, t_i32, i, scale_factor);
        b->new_dest_clause(b, if_true, blk2, IDARR(5, i, sum, scale_factor, n, cb), 5);
        b->new_dest_clause(b, if_false, blk3, IDARR(4, sum, n, scale_factor, cb), 4);
        b->new_branch2(b, op_br2, cmp_res, if_true, if_false);
        b->new_bb(b, blk1, IDARR(5, i, sum, scale_factor, n, cb),
                  IDARR(5, t_i32, t_i32, t_i32, t_i32, t_callback_p), 5,
                  MU_NO_ID, IDARR(2, op_SLT, op_br2), 2);
    }

    { // blk2
        MuID i, sum, scale_factor, n, cb, fib_res, sum_inc, i_suc;
        MuID ops[4];
        i = b->gen_sym(b, "@entry.v1.blk2.i");
        sum = b->gen_sym(b, "@entry.v1.blk2.sum");
        scale_factor = b->gen_sym(b, "@entry.v1.blk2.scale_factor");
        n = b->gen_sym(b, "@entry.v1.blk2.n");
        cb = b->gen_sym(b, "@entry.v1.blk2.cb");
        fib_res = b->gen_sym(b, "@entry.v1.blk2.fib_res");
        sum_inc = b->gen_sym(b, "@entry.v1.blk2.sum_inc");
        i_suc = b->gen_sym(b, "@entry.v1.blk2.i_suc");
        for (int _i = 0; _i < 4; _i ++ )
            ops[_i] = b->gen_sym(b, NULL);

        b->new_call(b, ops[0], IDARR(1, fib_res), 1, sig_fib, fib, IDARR(1, n), 1, MU_NO_ID, MU_NO_ID);
        b->new_binop(b, ops[1], sum_inc, MU_BINOP_ADD, t_i32, sum, fib_res, MU_NO_ID);
        b->new_binop(b, ops[2], i_suc, MU_BINOP_ADD, t_i32, i, c_1, MU_NO_ID);
        MuID dst = b->gen_sym(b, NULL);
        b->new_dest_clause(b, dst, blk1, IDARR(5, i_suc, sum_inc, scale_factor, n, cb), 5);
        b->new_branch(b, ops[3], dst);
        b->new_bb(b, blk2,
                  IDARR(5, i, sum, scale_factor, n, cb),
                  IDARR(5, t_i32, t_i32, t_i32, t_i32, t_callback_p), 5,
                  MU_NO_ID, ops, 4);
    }

    { // blk3
        MuID sum, n, scale_factor, cb, fib_n, expected, cmpres, retcode;
        MuID ops[9];
        sum = b->gen_sym(b, "@entry.v1.blk3.sum");
        n = b->gen_sym(b, "@entry.v1.blk3.n");
        scale_factor = b->gen_sym(b, "@entry.v1.blk3.scale_factor");
        cb = b->gen_sym(b, "@entry.v1.blk3.cb");
        fib_n = b->gen_sym(b, "@entry.v1.blk3.fib_n");
        expected = b->gen_sym(b, "@entry.v1.blk3.expected");
        cmpres = b->gen_sym(b, "@entry.v1.blk3.cmpres");
        retcode = b->gen_sym(b, "@entry.v1.blk3.retcode");
        for (int i = 0; i < 9; i ++)
            ops[i] = b->gen_sym(b, NULL);

        b->new_ccall(b, ops[0], NULL, 0, MU_CC_DEFAULT,
                     t_fp_cb_end, sig_cb_end, extern_cb_end,
                     IDARR(1, cb), 1, MU_NO_ID, MU_NO_ID);
        b->new_ccall(b, ops[1], NULL, 0, MU_CC_DEFAULT,
                     t_fp_cb_report, sig_cb_report, extern_cb_report,
                     IDARR(1, cb), 1, MU_NO_ID, MU_NO_ID);
        b->new_ccall(b, ops[2], NULL, 0, MU_CC_DEFAULT,
                     t_fp_cb_finish, sig_cb_finish, extern_cb_finish,
                     IDARR(1, cb), 1, MU_NO_ID, MU_NO_ID);
        b->new_call(b, ops[3], IDARR(1, fib_n), 1, sig_fib, fib,
                    IDARR(1, n), 1, MU_NO_ID, MU_NO_ID);
        b->new_binop(b, ops[4], expected, MU_BINOP_MUL, t_i32, fib_n, scale_factor, MU_NO_ID);
        b->new_cmp(b, ops[5], cmpres, MU_CMP_NE, t_i32, sum, expected);
        b->new_conv(b, ops[6], retcode, MU_CONV_ZEXT, t_i1, t_i32, cmpres);

        b->new_ccall(b, ops[7], NULL, 0, MU_CC_DEFAULT, t_fp_exit, sig_exit, extern_exit,
                     IDARR(1, retcode), 1, MU_NO_ID, MU_NO_ID);
        b->new_ret(b, ops[8], IDARR(1, c_0), 1);
        b->new_bb(b, blk3,
                  IDARR(4, sum, n, scale_factor, cb),
                  IDARR(4, t_i32, t_i32, t_i32, t_callback_p), 4,
                  MU_NO_ID, ops, 9);
    }
    b->new_func(b, entry, sig_main);
    b->new_func_ver(b, v1, entry, IDARR(4, blk0, blk1, blk2, blk3), 4);
}

void mubench_build_heap(MuCtx *ctx) {} // no heap needed

int mubench_get_benchmark_global_defs(MuID **parr)
{
    MuID ids[] = {t_i1,
                  t_i32,
                  t_hccharp,
                  t_ccharpp,
                  t_pccharp,
                  t_fp_atoi,
                  t_fp_exit,
                  c_0,
                  c_1,
                  c_2,
                  c_3,
                  c_4,
                  sig_fib,
                  sig_main,
                  sig_atoi,
                  sig_exit,
                  extern_atoi,
                  extern_exit,
                  fib,
                  entry};

    *parr = (MuID *)malloc(sizeof(ids));
    memcpy(*parr, ids, sizeof(ids));
    return sizeof(ids) / sizeof(MuID);
}

void mubench_get_reloc_info(MuIRefValue **sym_flds,
                            MuCString **sym_strs,
                            MuArraySize *nsyms,
                            MuIRefValue **reloc_flds,
                            MuCString **reloc_strs,
                            MuArraySize *nrelocs)
{
    *sym_flds = NULL;
    *sym_strs = NULL;
    *nsyms = 0;
    *reloc_flds = NULL;
    *reloc_strs = NULL;
    *nrelocs = 0;
}
