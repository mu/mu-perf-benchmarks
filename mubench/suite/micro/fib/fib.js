function fib(n) {
    if (n === 1 || n === 0) {
        return 1;
    }
    return fib(n - 1) + fib(n - 2);
}

const startTime = process.hrtime();
console.log(fib(35));
const diffTime = process.hrtime(startTime);
console.log(String(diffTime[0] + diffTime[1] / Math.pow(10, 9)));