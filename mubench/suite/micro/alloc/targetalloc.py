#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class A:
    pass


def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        cb = cb_cls(cb_param)

        # program args
        scale_factor = int(argv[2])
        n = int(argv[3])

        lst = [[None] * n] * scale_factor

        cb.begin()
        for j in range(scale_factor):
            l = lst[j]
            for k in range(n):
                l[k] = A()
        cb.end()
        cb.report()
        # return int(not (len(lst) == n * scale_factor))
        return 0

    return main, None
