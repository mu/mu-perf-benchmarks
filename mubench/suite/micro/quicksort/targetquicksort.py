#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# algorithm taken from Wikipedia
def swap(arr, i, j):
    t = arr[i]
    arr[i] = arr[j]
    arr[j] = t


def partition(arr, idx_low, idx_high):
    pivot = arr[idx_high]
    i = idx_low
    for j in range(idx_low, idx_high):
        if arr[j] < pivot:
            swap(arr, i, j)
            i += 1
    swap(arr, i, idx_high)
    return i


def quicksort(arr, start, end):
    if start < end:
        p = partition(arr, start, end)
        quicksort(arr, start, p - 1)
        quicksort(arr, p + 1, end)


RANDINTS = 1000  # there are 1000 32-bit integers in randints file


def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    scale_factor = int(args[1])

    def check_sorted_list(lst):
        correct = True
        for i in range(1, RANDINTS):
            correct &= lst[i - 1] < lst[i]
        return correct

    # read predefined random integers from randints file.
    import os
    from copy import copy
    from struct import unpack
    randints_path = os.path.abspath(os.path.join(__file__, '..', 'randints'))
    randints = []
    with open(randints_path, 'rb') as fp:
        for i in range(RANDINTS):
            randints.append(unpack('i', fp.read(4))[0])

    ints = randints[:RANDINTS]
    lists = [ints] * scale_factor

    def main(argv):
        cb_param = argv[1]

        cb = cb_cls(cb_param)
        idx = 0

        cb.begin()
        for i in range(scale_factor):
            lst = lists[idx]
            quicksort(lst, 0, RANDINTS - 1)
            idx += 1
        cb.end()
        correct = check_sorted_list(lists[idx - 1])
        cb.report()

        return int(not correct)

    return main, None
