# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os


def eprint(msg):
    os.write(2, str(msg))

def except_raiser(n, c):
    if n == 0:
        if c != 0:
            raise Exception('except')
            eprint(3)
        else:
            eprint(4)
            return 0
    else:
        eprint(5)
        return except_raiser(n - 1, c) + 1

def except_catcher(n, c):
    try:
        return except_raiser(n, c)
    except Exception:
        return 0

def fake_catcher(n):
    return fake_raiser(n)

def fake_raiser(n):
    if n == 0:
        eprint(4)
        return 0
    else:
        eprint(5)
        return fake_raiser(n - 1) + 1

def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        cb = cb_cls(cb_param)

        # program args
        recursions = int(argv[2])
        c = int(argv[3])
        scale_factor = int(argv[4])

        lst = []
        if c > 1:
            cb.begin()
            for i in range(scale_factor):
               eprint(fake_catcher(recursions))
            cb.end()
        else:
            cb.begin()
            for i in range(scale_factor):
                eprint(except_catcher(recursions, c))
            cb.end()
        cb.report()
        return 0

    return main, None
