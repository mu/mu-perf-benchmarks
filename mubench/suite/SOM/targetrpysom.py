# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def target(driver, args):

    from callback import get_callback
    cb_cls = get_callback(args[0])

    import os
    from som.vm.universe import main, Exit

    def entry_point(argv):
        cb_param = argv[1]
        cb = cb_cls(cb_param)

        scale_factor = int(argv[2])
        cb.begin()
        retcode = 1
        for i in range(scale_factor):
            try:
                main([argv[0]] + argv[3:])
            except Exit, e:
                retcode = e.code
            except Exception, e:
                os.write(2, "ERROR: %s thrown during execution.\n" % e)
                retcode = 1
        cb.end()
        cb.report()

        return retcode


    return entry_point, None
