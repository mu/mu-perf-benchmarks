# Adaptation of the C version in fannkuchredux.c
# author: John Zhang



def fannkuchredux(n):
    perm = [0] * n
    perm1 = [i for i in range(n)]
    count = [0] * n

    maxFlipsCount = 0
    permCount = 0

    r = n

    while True:
        while r != 1:
            count[r-1] = r
            r -= 1

        for i in range(n):
            perm[i] = perm1[i]

        flipsCount = 0

        k = perm[0]
        while k != 0:
            k2 = (k + 1) >> 1
            for i in range(k2):
                temp = perm[i]
                perm[i] = perm[k-i]
                perm[k-i] = temp

            flipsCount += 1
            k = perm[0]

        maxFlipsCount = maxFlipsCount if (maxFlipsCount > flipsCount) else flipsCount

        while True:
            if r == n:
                return maxFlipsCount

            perm0 = perm1[0]
            i = 0
            while i < r:
                j = i + 1
                perm1[i] = perm1[j]
                i = j

            perm1[r] = perm0
            count[r] = count[r] - 1
            if count[r] > 0:
                break

            r += 1

        permCount += 1

def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        n = int(argv[2]) if len(argv) > 2 else 7

        cb = cb_cls(cb_param)

        cb.begin()
        res = fannkuchredux(n)
        cb.end()
        print "fannkuchredux(%d) = %d" % (n, res)
        cb.report()
        return 0

    return main, None
