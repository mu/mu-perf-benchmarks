/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * converted to C by Joseph Piché
 * from Java version by Oleg Mazurov and Isaac Gouy
 *
 * Modified by John Zhang
 */

#include <stdio.h>
#include <stdlib.h>
#include "callback.h"


int fannkuchredux(int n)
{
    int perm[n];
    int perm1[n];
    int count[n];

    int maxFlipsCount = 0;
    int permCount = 0;

    int i;

    for (i=0; i<n; i+=1)
        perm1[i] = i;
    int r = n;

    while (1) {
        while (r != 1) {
            count[r-1] = r;
            r -= 1;
        }

        for (i=0; i<n; i+=1)
            perm[i] = perm1[i];
        int flipsCount = 0;
        int k;

        while ( !((k = perm[0]) == 0) ) {
            int k2 = (k+1) >> 1;
            for (i=0; i<k2; i++) {
                int temp = perm[i]; perm[i] = perm[k-i]; perm[k-i] = temp;
            }
            flipsCount += 1;
        }

        maxFlipsCount = (maxFlipsCount > flipsCount) ? maxFlipsCount : flipsCount;

        /* Use incremental change to generate another permutation */
        while (1) {
            if (r == n) {
                return maxFlipsCount;
            }

            int perm0 = perm1[0];
            i = 0;
            while (i < r) {
                int j = i + 1;
                perm1[i] = perm1[j];
                i = j;
            }
            perm1[r] = perm0;
            count[r] = count[r] - 1;
            if (count[r] > 0) break;
            r++;
        }
        permCount++;
    }
}

int main(int argc, char *argv[])
{
    struct Callback* cb;
    int n, res;

    cb = cb_init(argv[1]);
    n = argc > 2 ? atoi(argv[2]) : 7;

    cb_begin(cb, NULL);
    res = fannkuchredux(n);
    cb_end(cb, NULL);
    printf("fannkuchredux(%d) = %d\n", n, res);
    cb_report(cb);
    return 0;
}
