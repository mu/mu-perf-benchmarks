# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org/
#
# Contributed by Sebastien Loisel
# Fixed by Isaac Gouy
# Sped up by Josh Goldfoot
# Dirtily sped up by Simon Descarpentries
# Used list comprehension by Vadim Zelenin
# 2to3

from math      import sqrt
from sys       import argv


def eval_A(i, j):
    ij = i+j
    return 1.0 / (ij * (ij + 1) / 2 + i + 1)


def eval_A_times_u(u):
    local_eval_A = eval_A

    ret = []
    for i in range(len(u)):
        sum = 0;
        for j, u_j in enumerate(u):
            sum += local_eval_A(i, j) * u_j
        ret.append(sum)

    return ret


def eval_At_times_u(u):
    local_eval_A = eval_A

    ret = []
    for i in range(len(u)):
        sum = 0
        for j, u_j in enumerate(u):
            sum += local_eval_A(j, i) * u_j
        ret.append(sum)

    return ret


def eval_AtA_times_u(u):
    return eval_At_times_u(eval_A_times_u(u))


def bm_main(n):
    u = [1] * n
    v = []
    local_eval_AtA_times_u = eval_AtA_times_u

    for dummy in range(10):
        v = local_eval_AtA_times_u(u)
        u = local_eval_AtA_times_u(v)

    vBv = vv = 0

    for i in range(len(u)):
        ue = u[i]
        ve = v[i]

        vBv += ue * ve
        vv  += ve * ve

    # print("%d" % (sqrt(vBv/vv)))
    print("%d" % len(u))

def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        N = int(argv[2])

        cb = cb_cls(cb_param)

        cb.begin()
        bm_main(N)
        cb.end()
        cb.report()
        return 0

    return main, None