# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org/
#
# contributed by Rene Bakker
# fixed by Isaac Gouy

import sys
from rpython.rlib.rStringIO import RStringIO

def bm_main(N):
    f = RStringIO()

    w = int(0)
    k = int(1)

    n1  = int(4)
    n2  = int(3)
    d   = int(1)
    f10 = int(10)
    n10 = int(-10)

    i = int(0)
    while True:
        # digit
        u = n1 / d
        v = n2 / d
        if u == v:
            f.write(chr(48+u))
            i += 1
            if i % 10 == 0:
                f.write("\t:%d\n" % i)

            if i == N:
                break

            # extract
            u  = d * (n10 * u)
            n1 = n1 * f10
            n1 = n1 + u
            n2 = n2 * f10
            n2 = n2 + u
        else:
            # produce
            k2 = k << 1
            u  = n1 * (k2 - 1)
            v  = n2 + n2
            w  = n1 * (k - 1)
            n1 = u + v
            u  = n2 * (k + 2)
            n2 = w + u
            d  = d * (k2 + 1)
            k += 1;

    if i % 10 != 0:
        f.write("%s\t:%d\n" % (' ' * (10 - (i%10)), N))
    print f.getvalue()

def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        N = int(argv[2])

        cb = cb_cls(cb_param)

        cb.begin()
        bm_main(N)
        cb.end()
        # print "total check = %d" % check
        cb.report()
        return 0

    return main, None