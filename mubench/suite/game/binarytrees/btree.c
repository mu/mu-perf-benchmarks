/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/

   contributed by Kevin Carson
   compilation:
       gcc -O3 -fomit-frame-pointer -funroll-loops -static binary-trees.c -lm
       icc -O3 -ip -unroll -static binary-trees.c -lm

   *reset*
   modified by John Zhang
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "callback.h"

typedef struct tn {
    struct tn*    left;
    struct tn*    right;
} treeNode;


#ifdef BTREE_DELETE_TREE_SETNULL
#define DELETE_TREE(t) t = NULL
#else   // default to call free
#define DELETE_TREE(t) DeleteTree(t)
#endif


treeNode* NewTreeNode(treeNode* left, treeNode* right)
{
    treeNode*    new;

    new = (treeNode*)malloc(sizeof(treeNode));

    new->left = left;
    new->right = right;

    return new;
} /* NewTreeNode() */

treeNode* BottomUpTree(unsigned long depth)
{
    if (depth > 0)
        return NewTreeNode(BottomUpTree(depth - 1), BottomUpTree(depth - 1));
    else
        return NewTreeNode(NULL, NULL);
} /* BottomUpTree() */

unsigned long ItemCheck(treeNode* tree)
{
    if (tree->left == NULL)
        return 1;
    else
        return 1 + ItemCheck(tree->left) + ItemCheck(tree->right);
} /* ItemCheck() */


void DeleteTree(treeNode* tree)
{
    if (tree->left != NULL)
    {
        DeleteTree(tree->left);
        DeleteTree(tree->right);
    }

    free(tree);
} /* DeleteTree() */

unsigned long bm_main(unsigned long N)
{
    unsigned long minDepth, maxDepth, stretchDepth;
    treeNode   *stretchTree, *longLivedTree, *tempTree;
    unsigned long check, iterations;

    minDepth = 4;
    maxDepth = (minDepth + 2) > N ? (minDepth + 2) : N;
    stretchDepth = maxDepth + 1;
    check = 0;

    stretchTree = BottomUpTree(stretchDepth);
    check += ItemCheck(stretchTree);
    DELETE_TREE(stretchTree);

    longLivedTree = BottomUpTree(maxDepth);

    for (unsigned long depth = minDepth; depth <= maxDepth; depth += 2)
    {
        iterations = 1 << (maxDepth + minDepth - depth);

        for (unsigned long i = 1; i <= iterations; i++)
        {
            tempTree = BottomUpTree(depth);
            check += ItemCheck(tempTree);
            DELETE_TREE(tempTree);
            tempTree = NULL;
        } /* for(i = 1...) */
    } /* for(depth = minDepth...) */
    check += ItemCheck(longLivedTree);
    DELETE_TREE(longLivedTree);
    return check;
}

int main(int argc, char* argv[])
{
    unsigned long N, check;
    struct Callback* cb;

    cb = cb_init(argv[1]);
    N = atol(argv[2]);

    cb_begin(cb, NULL);
    check = bm_main(N);
    cb_end(cb, NULL);
    printf("total check = %li\n", check);
    cb_report(cb);
    return 0;
} /* main() */
