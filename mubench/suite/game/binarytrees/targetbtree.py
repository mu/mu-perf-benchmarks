# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org/
#
# contributed by Antoine Pitrou
# modified by Dominique Wahli and Daniel Nanz
# modified by Joerg Baumann
# modified by John Zhang into RPython, following the pattern of the C equivalent
class Node:
    def __init__(self, l, r):
        self.l = l
        self.r = r


def bottom_up_tree(d):
    if d > 0:
        return Node(bottom_up_tree(d - 1), bottom_up_tree(d - 1))
    return Node(None, None)


def item_check(node):
    l = node.l
    r = node.r
    if l is None:
        return 1
    else:
        return 1 + item_check(l) + item_check(r)


def bm_main(N, min_depth=4):
    minDepth = 4
    maxDepth = (minDepth + 2) if (minDepth + 2) > N else N
    stretchDepth = maxDepth + 1
    check = 0

    stretchTree = bottom_up_tree(stretchDepth)
    check += item_check(stretchTree)
    stretchTree = None  # rely on GC to free memory

    longLivedTree = bottom_up_tree(maxDepth)

    depth = minDepth
    while(depth <= maxDepth):
        iterations = 1 << (maxDepth + minDepth - depth)

        i = 1
        while(i <= iterations):
            tempTree = bottom_up_tree(depth)
            check += item_check(tempTree)
            tempTree = None
            i += 1
        depth += 2

    check += item_check(longLivedTree)
    longLivedTree = None
    return check


def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        N = int(argv[2])

        cb = cb_cls(cb_param)

        cb.begin()
        check = bm_main(N)
        cb.end()
        print "total check = %d" % check
        cb.report()
        return 0

    return main, None
