import sys
from math import sqrt

def combinations(l):
    result = []
    for x in range(len(l) - 1):
        ls = l[x+1:]
        for y in ls:
            result.append(BodyPair(l[x], y))
    return result

PI = 3.14159265358979323
SOLAR_MASS = 4 * PI * PI
DAYS_PER_YEAR = 365.24

class Body:
    def __init__(self, x, y, z, vx, vy, vz, mass):
        self.x = x
        self.y = y
        self.z = z
        self.vx = vx
        self.vy = vy
        self.vz = vz
        self.mass = mass

class BodyPair:
    def __init__(self, body1, body2):
        self.body1 = body1
        self.body2 = body2

BODIES = {
    'sun': Body(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, SOLAR_MASS),

    'jupiter': Body(4.84143144246472090e+00,
                    -1.16032004402742839e+00,
                    -1.03622044471123109e-01,
                    1.66007664274403694e-03 * DAYS_PER_YEAR,
                    7.69901118419740425e-03 * DAYS_PER_YEAR,
                    -6.90460016972063023e-05 * DAYS_PER_YEAR,
                    9.54791938424326609e-04 * SOLAR_MASS),

    'saturn': Body(8.34336671824457987e+00,
                   4.12479856412430479e+00,
                   -4.03523417114321381e-01,
                   -2.76742510726862411e-03 * DAYS_PER_YEAR,
                   4.99852801234917238e-03 * DAYS_PER_YEAR,
                   2.30417297573763929e-05 * DAYS_PER_YEAR,
                   2.85885980666130812e-04 * SOLAR_MASS),

    'uranus': Body(1.28943695621391310e+01,
                   -1.51111514016986312e+01,
                   -2.23307578892655734e-01,
                   2.96460137564761618e-03 * DAYS_PER_YEAR,
                   2.37847173959480950e-03 * DAYS_PER_YEAR,
                   -2.96589568540237556e-05 * DAYS_PER_YEAR,
                   4.36624404335156298e-05 * SOLAR_MASS),

    'neptune': Body(1.53796971148509165e+01,
                    -2.59193146099879641e+01,
                    1.79258772950371181e-01,
                    2.68067772490389322e-03 * DAYS_PER_YEAR,
                    1.62824170038242295e-03 * DAYS_PER_YEAR,
                    -9.51592254519715870e-05 * DAYS_PER_YEAR,
                    5.15138902046611451e-05 * SOLAR_MASS)
}

SYSTEM = [BODIES['sun'], BODIES['jupiter'], BODIES['saturn'], BODIES['uranus'], BODIES['neptune']]
PAIRS = combinations(SYSTEM)

def advance(dt, n, bodies=SYSTEM, pairs=PAIRS):
    for i in range(n):
        for body_pair in pairs:
            body1 = body_pair.body1
            body2 = body_pair.body2

            x1 = body1.x
            y1 = body1.y
            z1 = body1.z
            m1 = body1.mass

            x2 = body2.x
            y2 = body2.y
            z2 = body2.z
            m2 = body2.mass

            dx = x1 - x2
            dy = y1 - y2
            dz = z1 - z2
            dist = sqrt(dx * dx + dy * dy + dz * dz);
            mag = dt / (dist*dist*dist)
            b1m = m1 * mag
            b2m = m2 * mag
            body1.vx -= dx * b2m
            body1.vy -= dy * b2m
            body1.vz -= dz * b2m
            body2.vz += dz * b1m
            body2.vy += dy * b1m
            body2.vx += dx * b1m
        for body in bodies:
            body.x += dt * body.vx
            body.y += dt * body.vy
            body.z += dt * body.vz

def report_energy(bodies=SYSTEM, pairs=PAIRS, e=0.0):
    for body_pair in pairs:
        body1 = body_pair.body1
        body2 = body_pair.body2

        x1 = body1.x
        y1 = body1.y
        z1 = body1.z
        m1 = body1.mass

        x2 = body2.x
        y2 = body2.y
        z2 = body2.z
        m2 = body2.mass

        dx = x1 - x2
        dy = y1 - y2
        dz = z1 - z2
        e -= (m1 * m2) / (sqrt(dx * dx + dy * dy + dz * dz))
    for body in bodies:
        vx = body.vx
        vy = body.vy
        vz = body.vz
        m = body.mass

        e += m * (vx * vx + vy * vy + vz * vz) / 2.
    print("%f" % e)

def offset_momentum(ref, bodies=SYSTEM, px=0.0, py=0.0, pz=0.0):
    for body in bodies:
        vx = body.vx
        vy = body.vy
        vz = body.vz
        m = body.mass

        px -= vx * m
        py -= vy * m
        pz -= vz * m

    ref.vx = px / ref.mass
    ref.vy = py / ref.mass
    ref.vz = pz / ref.mass

def bm_main(n, ref='sun'):
    offset_momentum(BODIES[ref])
    report_energy()
    advance(0.01, n)
    report_energy()

def target(driver, args):
    from callback import get_callback
    cb_cls = get_callback(args[0])

    def main(argv):
        cb_param = argv[1]
        N = int(argv[2])

        cb = cb_cls(cb_param)

        cb.begin()
        bm_main(N)
        cb.end()
        cb.report()
        return 0

    return main, None