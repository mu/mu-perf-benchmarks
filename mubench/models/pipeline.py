#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
import sys

import crayons

logger = logging.getLogger(__name__)


class Pipeline:
    def process(self, report):
        pass


class LogOutputPipeline(Pipeline):
    def process(self, tasksets):
        for ts in tasksets:
            description = ["", ""]
            for task_name, result in ts.results.items():
                # task_name: str, "c_O3"
                # result: dict<str, Trails>
                description.append("\n".join([str(x) for x in result.values()]))
                description.append("")
            for comp in ts.comparison:
                r1 = ts.results[comp.op1]  # dict<str, Trails>
                r2 = ts.results[comp.op2]  # dict<str, Trails>
                for metric in r1.keys():
                    description.append(self.compare(r1[metric], r2[metric]))
            logger.info("\n".join(description))
        return tasksets

    @staticmethod
    def compare(r1, r2):
        decreased, changed, increased = r1.compare(r2)
        if r1.mean < r2.mean:
            ratio = "{:.2f} times faster".format(r2.mean / r1.mean)
        else:
            ratio = "{:.2f} times slower".format(r1.mean / r2.mean)
        if decreased:
            trend = "It's statistically significantly " + \
                    crayons.red("lower") + " using one-sided t-test"
        elif increased:
            trend = "It's statistically significantly " + \
                    crayons.red("higher") + " using one-sided t-test"
        elif changed:
            trend = "There's statistically significant " + \
                    "difference using two-sided t-test"
        else:
            trend = "There's no statistically significant difference"
        template = "{} is {} than {}, using {} of time\n\t{}"

        return template.format(crayons.blue(r1.name),
                               crayons.green(ratio),
                               crayons.blue(r2.name),
                               crayons.green(
                                   "{:.4%}".format(r1.mean / r2.mean)),
                               trend)


class BarplotPipeline(Pipeline):
    def process(self, tasksets):
        def autolabel(rects):
            for rect in rects:
                height = rect.get_height()
                xpostion = rect.get_x() + rect.get_width() / 2
                ypostion = 1.05 * height
                ax.text(xpostion,
                        ypostion,
                        "{:.3f}".format(height),
                        ha='center', va='bottom')

        import numpy as np
        import matplotlib
        matplotlib.use('TkAgg')
        import matplotlib.pyplot as plt
        for idx, ts in enumerate(tasksets):
            tasks = list(ts.results.items())  # [(str, dict)]
            if not tasks:
                logger.error("No task")
                sys.exit(1)
            keys = tasks[0][1].keys()
            for key in keys:
                items = sorted([v[key] for k, v in ts.results.items()],
                               key=lambda x: x.mean)
                n = len(items)
                means = [x.mean for x in items]
                stds = [x.std for x in items]
                labels = [x.name for x in items]
                ind = np.arange(n)
                width = 0.35

                fig, ax = plt.subplots()
                rects = ax.bar(ind, means, width,
                               color='lightblue',
                               yerr=stds,
                               capsize=3)
                ax.set_ylabel(key)
                ax.set_title(ts.name)
                ax.set_xticks(ind)
                ax.set_xticklabels(labels)
                autolabel(rects)
        plt.show()

        return tasksets


class BoxplotPipeline(Pipeline):
    def process(self, tasksets):
        import numpy as np
        import matplotlib
        matplotlib.use('TkAgg')
        import matplotlib.pyplot as plt
        for idx, ts in enumerate(tasksets):
            tasks = list(ts.results.items())  # [(str, dict)]
            if not tasks:
                logger.error("No task")
                sys.exit(1)
            keys = tasks[0][1].keys()
            for key in keys:
                plt.figure()
                items = sorted([v[key] for k, v in ts.results.items()],
                               key=lambda x: x.mean)
                labels = [x.name for x in items]
                vectors = [x.raw for x in items]
                bplot = plt.boxplot(vectors, labels=labels, patch_artist=True)
        plt.show()
        return tasksets


pipelines = {
    "box": BoxplotPipeline,
    "bar": BarplotPipeline,
    "log": LogOutputPipeline
}
