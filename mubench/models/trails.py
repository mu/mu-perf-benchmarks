#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import crayons
from mubench.stats import mean, median, std, significant_change, \
    significant_decrease, significant_increase
from types import SimpleNamespace


class Trails:
    """
    Trails consists of raw data points and our analysis.
    It can be specified by a combination of Revision and Task.
    """

    def __init__(self, name, data_points):
        self.name = name
        self.raw = data_points
        self.mean = mean(data_points)
        self.median = median(data_points)
        self.std = std(data_points)
        self.description = "{} {:.9f} (±{:.9f})".format(
            crayons.blue(self.name),
            self.mean,
            self.std
        )

    def compare(self, prev_res):
        prev_mean = prev_res.mean
        decreased = significant_decrease(prev_mean, self.raw)
        increased = significant_increase(prev_mean, self.raw)
        changed = significant_change(prev_mean, self.raw)
        return decreased, changed, increased

    def compare_obj(self, prev_res):
        decreased, changed, increased = self.compare(prev_res)
        return SimpleNamespace(
            decreased=decreased,
            changed=changed,
            increased=increased,
            relative_diff=self.mean / prev_res.mean,
            relative_diff_str="{:+.2%}".format(self.mean / prev_res.mean - 1),
            absolute_diff=self.mean - prev_res.mean,
            mean=self.mean,
            std=self.std
        )

    def __str__(self):
        return self.description
