#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Functions to configure callback settings
import sys
import logging
from pathlib import Path

import crayons

from mubench import CALLBACKS_DIR
from mubench.utils import expandenv, run_in_subproc, ExecutionFailure

logger = logging.getLogger(__name__)


class Callback:
    def __init__(self, conf_d, ts):
        for key in conf_d:
            setattr(self, key, conf_d[key])

        libext = '.dylib' if sys.platform == 'darwin' else '.so'
        self.dylib = ts.output_dir / ('libcb_%(name)s' % conf_d + libext)

        self.configure(ts)

    def configure(self, ts):
        pass

    def compile_dylib(self, output_dir, env):
        cmd = []

        cmd.append(env.get('CC', 'clang'))

        cmd.extend(['-shared', '-fPIC'])

        # include_dirs
        cmd.extend(map(lambda s: '-I' + s, self.include_dirs))

        # library_dirs
        cmd.extend(map(lambda s: '-L' + s, self.library_dirs))

        # libraries
        cmd.extend(map(lambda s: '-l' + s, self.libraries))

        # flags
        cmd.extend(self.flags)

        # output
        cmd.extend(['-o', self.dylib])

        # source
        cmd.append(CALLBACKS_DIR / ('cb_%s.c' % self.name))
        cmd.extend(self.extra_srcs)

        try:
            run_in_subproc(cmd, env)
        except ExecutionFailure as e:
            logger.info('Compiling callback library ' + crayons.red('FAILED'))
            logger.critical(crayons.red(str(e)))
            errlog_file = output_dir / 'libcb.log'
            e.dump(errlog_file)
            logger.info('error output written to %s' % errlog_file)
            exit(1)

        return self.dylib

    def extract_stat(self, stdout, stderr):
        pass

    def __getitem__(self, key):
        return getattr(self, key)


class ClockCallback(Callback):
    def extract_stat(self, stdout, stderr):
        return {'time': float(stdout.split()[-1])}


class PrintCallback(Callback):
    pass


class PerfEventCallback(Callback):
    def configure(self, ts):
        assert hasattr(self, 'libpfm4_dir'), "libpfm4 install directory not specified"
        libpfm4 = Path(expandenv(self.libpfm4_dir, ts.env))
        if not libpfm4.is_absolute():
            libpfm4 = CALLBACKS_DIR / libpfm4   # default relative to CALLBACKS_DIR

        self.include_dirs.extend([
            str(libpfm4 / 'include'),
            str(libpfm4 / 'util')
        ])
        self.library_dirs.append(str(libpfm4 / 'lib'))
        self.libraries.append('pfm')
        self.extra_srcs.append(str(libpfm4 / 'util' / 'perf_util.c'))
        self.flags.append('-pthread')

    def extract_stat(self, stdout, stderr):
        lines = stdout.split('\n')
        idx_start = lines.index('============================ Tabulate Statistics ============================')
        idx_end = lines.index('=============================================================================')
        events = lines[idx_start + 1].split('\t')
        data = list(map(int, lines[idx_start + 2].split('\t')[:-1]))
        return dict(zip(events, data))


CALLBACK_BY_NAME = {
    'clock': ClockCallback,
    'print': PrintCallback,
    'perfevent': PerfEventCallback,
}
