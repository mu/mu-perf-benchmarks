#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mubench.models.taskset import load_file


class Revision:
    """
    A revision is a snapshot of a project.
    It has a hash string, time of creation, and other metadata (branch, etc.).
    """

    def __init__(self, **kwargs):
        for k in kwargs:
            setattr(self, k, kwargs[k])

    def run_taskset(self):
        """
        Run tasks and store the result as attribute
        """
        pass

    def store_result(self):
        """
        Store the result to database
        :return:
        """
        pass


class LocalRevision(Revision):
    def __init__(self, file, **kwargs):
        super().__init__(**kwargs)
        self.file = file
        self.tasksets = load_file(self.file)

    def run_tasksets(self, skipcomp_s):
        """
        Run tasks and store the result as attribute
        """
        # TODO: fix skip compilation
        skipcomp_d = self.get_skipcomp_dic(skipcomp_s)
        for ts in self.tasksets:
            ts.run(skipcomp_d[ts.name])

    def get_skipcomp_dic(self, skipcomp_s):
        none_d = {ts.name: [] for ts in self.tasksets}
        all_d = {ts.name: [t.name for t in ts.tasks] for ts in self.tasksets}
        if skipcomp_s == 'none':
            return none_d
        elif skipcomp_s == 'all':
            return all_d
        else:
            skipcomp_d = none_d
            for sub in skipcomp_s.split(';'):
                if not ':' in sub:  # only the taskset name -> skip compilation of all tasks
                    skipcomp_d[sub] = all_d[sub]
                else:
                    tup = sub.split(':')
                    skipcomp_d[tup[0]] = tup[1].split(',')
            return skipcomp_d

    def __str__(self):
        return "LocalRevision({})".format(self.file)


class GitRevision(Revision):
    pass
