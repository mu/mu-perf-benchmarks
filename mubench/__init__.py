#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import os
from pathlib import Path
from pkg_resources import resource_isdir, resource_filename

__VERSION__ = "0.0.1"

assert sys.version_info[:2] >= (3, 4), "Python 3.4+ is required."
assert os.name == "posix", "Only *nix systems are supported."

if resource_isdir(__name__, "suite"):
    SUITE_DIR = Path(resource_filename(__name__, "suite"))
    CALLBACKS_DIR = Path(resource_filename(__name__, "callbacks"))
else:
    SUITE_DIR = Path(__file__) / 'suite'
    CALLBACKS_DIR = Path(__file__) / 'callbacks'
