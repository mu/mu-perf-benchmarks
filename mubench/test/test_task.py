#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import yaml
from mubench.models.task import Task
from mubench.models.taskset import TaskSet


def test_rpyc():
    config = yaml.load(
        """
        fib_rpy_c:
          environ:
            MUBENCH_RUN_DIR: /tmp
          benchmark:
            name: micro/fib
            iterations: 10
            args:
              - 35
            source: targetfib.py
            result: fib_rpy_c_data
          language:
            name: rpython
            backend: c  # required
          callback:
            name: ClockCallback
            param: "6"
          compiler:
            pypy_dir: /Users/johnz/Documents/Work/mu-client-pypy  # required by rpython
            flags:
              - -O3
              - --no-shared
        """)

    task = Task('fib_rpy_c', **config['fib_rpy_c'])
    task.run()


def test_rpymu_holstein():
    config = yaml.load(
        """
        test_rpymu_holstein:
          benchmark:
            name: micro/fib
            iterations: 10
            args:
              - 35
            source: targetfib.py
            result: fib_rpymu_holstein_data
          language:
            name: rpython
            backend: mu
            impl: holstein
          callback:
            name: ClockCallback
            param: "6"
          environ:  # can define environment variables that are passed to compiler and runner
            PYPY_MU:  /Users/johnz/Documents/Work/mu-client-pypy
            MU_HOLSTEIN: /Users/johnz/Documents/Work/mu-impl-ref2
            MUBENCH_RUN_DIR: /tmp
          compiler:
            pypy_dir: $PYPY_MU  # can use environment variables in paths
            flags:
              - --mu-vmargs=vmLog=ERROR
          runner:               # optional runner spec for non-ELF executable compilation targets
            flags:
              - --vmLog=ERROR
        """)

    task = Task('test_rpymu_holstein', **config['test_rpymu_holstein'])
    task.run()


def test_rpymu_zebu():
    config = yaml.load(
        """
        test_rpymu_zebu:
          benchmark:
            name: micro/fib
            iterations: 10
            args:
              - 35
            source: targetfib.py
            result: fib_rpymu_zebu_data
          language:
            name: rpython
            backend: mu
            impl: zebu
          callback:
            name: ClockCallback
            param: "6"
          environ:  # can define environment variables that are passed to compiler and runner
            PYPY_MU:  /Users/johnz/Documents/Work/mu-client-pypy
            MU_ZEBU: /Users/johnz/Documents/Work/mu-impl-fast
            MUBENCH_RUN_DIR: /tmp
          compiler:
            pypy_dir: $PYPY_MU  # can use environment variables in paths
            flags:
              - --mu-codegen=c
              - -O3
              - --no-shared
        """)

    task = Task('test_rpymu_zebu', **config['test_rpymu_zebu'])
    task.run()


def test_c():
    config = yaml.load(
        """
        test_fib_c:
          environ:
            MUBENCH_RUN_DIR: /tmp
          benchmark:
            name: micro/fib
            iterations: 10
            args:
              - 35
            source: fib.c
            result: fib_c_data
          language: c
          callback:
            name: clockcallback
            param: "6"    # precision
        """)

    task = Task('test_fib_c', **config['test_fib_c'])
    task.run()


def test_mu():
    config = yaml.load(
        """
        test_fib_mu:
          benchmark:
            name: micro/fib
            iterations: 10
            args:
              - 10
            source: build_fib.c
            result: fib_mu_data
          language:
            name: mu
            impl: holstein
          callback:
            name: clockcallback
            param: "6"
          environ:  # can define environment variables that are passed to compiler and runner
            PYPY_MU:  /Users/johnz/Documents/Work/mu-client-pypy
            MU_HOLSTEIN: /Users/johnz/Documents/Work/mu-impl-ref2
            MUBENCH_RUN_DIR: /tmp
          compiler:
            vm_args:
              - vmLog=ERROR
          runner:
            flags:
              - --vmLog=ERROR
        """)

    task = Task('test_fib_mu', **config['test_fib_mu'])
    task.run()


def test_mu_zebu():
    config = yaml.load(
        """
        test_fib_mu:
          benchmark:
            name: micro/fib
            iterations: 10
            args:
              - 10
            source: build_fib.c
            result: fib_mu_data
          language:
            name: mu
            impl: zebu
          callback:
            name: clockcallback
            param: "6"
          environ:  # can define environment variables that are passed to compiler and runner
            PYPY_MU:  /Users/johnz/Documents/Work/mu-client-pypy
            MU_ZEBU: /Users/johnz/Documents/Work/mu-impl-fast
            ZEBU_BUILD: release
            RUST_BACKTRACE: "1"
            MUBENCH_RUN_DIR: /tmp
        """)

    task = Task('test_fib_mu', **config['test_fib_mu'])
    task.run()


def test_python():
    config = yaml.load(
        """
        test_fib_python:
          environ:
            MUBENCH_RUN_DIR: /tmp
          benchmark:
            name: micro/fib    # required
            iterations: 10  # optional, default: 1
            args:        # optional, commandline arguments, default: none
              - 10
            source: fib.py      # optional, default: $(benchmark)/$(benchmark).$(language.ext)
            result: fib_py_data   # optional, default: $(task)_data
          language: python  # required
          callback:         # required, can be a string or a mapping
            name: ClockCallback # required, language defined. File placed in callbacks/$(language)/
            param: "6"          # optional, callback defined
          runner:           # optional, can be used to set different implementations
            exec: pypy
        """)

    task = Task('test_fib_python', **config['test_fib_python'])
    task.run()

def test_debug():
  config = """
quicksort:
  benchmark:
    name: micro/quicksort
    args:
      - &scale_factor 1000
  iterations: 50
  callback:
    name: clock
    param: "6"
  environ:
    PYPY_USESSION_DIR: example
  tasks:
    rpyc_O0:
      language:
        name: rpython
        backend: c
      source: targetquicksort.py
      environ:
        PYPY_C_CLANG_OPT: -O0
        PYPY_USESSION_BASENAME: &basename "${MUBENCH_TASKSET_NAME}_${MUBENCH_TASK_NAME}"
      flags:
        - --gc=none
      compiler:
        args:
          - *scale_factor
    rpyc_O3:
      language:
        name: rpython
        backend: c
      source: targetquicksort.py
      environ:
        PYPY_C_CLANG_OPT: -O3
        PYPY_USESSION_BASENAME: *basename
      flags:
        - --gc=none
      compiler:
        args:
          - *scale_factor
    rpyczebu:
      language:
        name: rpython
        backend: mu
        impl: zebu
      source: targetquicksort.py
      environ:
        PYPY_USESSION_BASENAME: *basename
      compiler:
        args:
          - *scale_factor
"""
  from pathlib import Path
  ts = TaskSet.from_yaml(config, Path("/tmp"))[0]
  assert ts.run()
