#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import time

from pathlib import Path

from mubench import CALLBACKS_DIR
from mubench.utils import expandenv
from mubench.utils import run_in_subproc

class Language:
    name = None
    src_ext = None
    compiled = True
    default_cc = 'clang'

    @classmethod
    def check_lang(cls, lc):
        return lc

    @classmethod
    def check_compiler(cls, cc, lc, task):
        return cc

    @classmethod
    def check_runner(cls, rc, lc, task):
        return rc

    @classmethod
    def get_default_target(cls, task):
        target_dir = task.output_dir
        target_name = '%s-%s' % (task.taskset.name, task.name)
        target = target_dir / target_name
        return target

    @classmethod
    def compile(cls, task):
        raise NotImplementedError

    @classmethod
    def run(cls, target, task):
        raise NotImplementedError


def get_lang(name):
    from mubench.lang.python import Python
    from mubench.lang.rpython import RPython
    from mubench.lang.c import C
    from mubench.lang.mu import Mu
    from mubench.lang.wasm import WASM
    lang_dict = {
        'python': Python,
        'rpython': RPython,
        'c': C,
        'mu': Mu,
        'wasm': WASM,
    }
    return lang_dict[name]
