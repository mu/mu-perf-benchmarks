#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import yaml

from mubench.models.task import Task


def test_rpython_c():
    config = yaml.load(
        """
        fib_rpy_c:
          benchmark: fib
          file: targetfib.py
          language: rpython
          callback: PrintCallback
          compiler:
            pypy_dir: /Users/johnz/Documents/Work/mu-client-pypy  # required by rpython
            backend: c  # required
            flags:
              - -O3
              - --no-shared
          prog_args:
            - 10
            - 1000000
        """)
    name = 'fib_rpy_c'
    conf = config[name]
    conf['run_dir'] = '/tmp'
    res = Task(name, **conf).run()


def test_rpython_mu_holstein():
    config = yaml.load(
        """
        fib_rpy_mu_holstein:
          benchmark: fib
          language: rpython
          file: targetfib.py
          callback: PrintCallback
          environ:
            PYPY_MU:  /Users/johnz/Documents/Work/mu-client-pypy
            MU_HOLSTEIN: /Users/johnz/Documents/Work/mu-impl-ref2
          compiler:
            pypy_dir: $PYPY_MU  # required by rpython
            backend: mu
            flags:
              - --mu-impl=holstein
              - --mu-codegen=c
              - --mu-vmargs=vmLog=ERROR
              - -O3
              - --no-shared
          runner:               # optional runner spec for non-ELF executable compilation targets
            exec: $MU_HOLSTEIN/tools/runmu.sh
            flags:
              - --vmLog=ERROR
          prog_args:
            - 10
            - 1000000
          result: fib_rpy_mu_holstein.json
        """)
    name = 'fib_rpy_mu_holstein'
    conf = config[name]
    conf['run_dir'] = '/tmp'
    res = Task(name, **conf).run()
