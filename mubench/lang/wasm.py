#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import time
import subprocess as subproc

from mubench.lang import Language
from mubench import CALLBACKS_DIR
from mubench.utils import expandenv, run_in_subproc
from types import SimpleNamespace

logger = logging.getLogger(__name__)


class WASM(Language):
    name = 'wasm'
    src_ext = 'c'
    compiled = True
    default_exec = 'emcc'  # assume presence of clang

    @classmethod
    def get_default_target(cls, task):
        target_dir = task.output_dir
        target_name = '%s-%s.js' % (task.taskset.name, task.name)
        target = target_dir / target_name
        return target

    @classmethod
    def check_compiler(cls, cc, lc, task):
        exe = cc.get('exec', cls.default_exec)
        cc['exec'] = expandenv(exe, task.env)
        return cc

    @classmethod
    def compile(cls, task):
        callback_dir = CALLBACKS_DIR
        cc = task.compiler

        cmd = []
        cmd.append(cc['exec'])

        # flags
        flags = ['-s', "WASM=1", '-D__WASM__']
        include_flags = ['-I%(callback_dir)s' % locals()]
        cc_flags = list(
            map(lambda a: expandenv(a, task.env), cc.get('flags', [])))
        flags.extend(cc_flags)
        flags.extend(include_flags)
        target = cls.get_default_target(task)
        flags.extend(['-o', str(target)])
        cmd.extend(flags)

        cmd.append(task.srcfile)

        # callback selected at compile time, initialised at runtime
        callback_file = callback_dir / ('cb_%(name)s.c' % task.callback)
        cmd.append(callback_file)

        run_in_subproc(cmd, task.env)

        assert target.exists()
        return target

    @classmethod
    def run(cls, target, task):
        cmd = ["node"]

        cmd.append(target.resolve())

        # first argument: callback param
        cmd.append(task.callback['param'])

        cmd.extend(task.benchmark['args'])

        return run_in_subproc(cmd, task.env,
                                  cwd=str(target.parent.resolve()))
