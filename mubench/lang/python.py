#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess as subproc
import os

from mubench import CALLBACKS_DIR
from mubench.lang import Language
from mubench.utils import expandenv, run_in_subproc


class Python(Language):
    name = 'python'
    src_ext = 'py'
    compiled = False
    default_exec = 'python'

    @classmethod
    def check_runner(cls, rc, lc, task):
        env = task.env
        # python interpreter
        python = rc.get('exec', cls.default_exec)
        rc['exec'] = expandenv(python, env)

        # PYTHOPATH
        pythonpath = env.get('PYTHONPATH', '')
        callback_dir = CALLBACKS_DIR / 'python'
        pythonpath = '%(callback_dir)s:%(pythonpath)s' % locals()
        env['PYTHONPATH'] = pythonpath

        return rc

    @classmethod
    def run(cls, target, task):
        rc = task.runner
        bmc = task.benchmark

        cmd = []

        cmd.append(rc['exec'])

        cmd.append(target)

        # first & second argument: callback class name and parameter string
        cmd.append(task.callback['dylib'])
        cmd.append(task.callback['param'])

        cmd.extend(bmc['args'])

        return run_in_subproc(cmd, task.env)
