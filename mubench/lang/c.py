#!/usr/bin/env python3
# Copyright 2017 The Australian National University
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from mubench.lang import Language
from mubench import CALLBACKS_DIR
from mubench.utils import expandenv, run_in_subproc


class C(Language):
    name = 'c'
    src_ext = 'c'
    compiled = True
    default_exec = 'clang'  # assume presence of clang

    @classmethod
    def check_compiler(cls, cc, lc, task):
        exe = cc.get('exec', cls.default_exec)
        cc['exec'] = expandenv(exe, task.env)
        return cc

    @classmethod
    def compile(cls, task):
        callback_dir = CALLBACKS_DIR
        cc = task.compiler

        cmd = []
        cmd.append(cc['exec'])

        # flags
        flags = []

        cc_flags = list(map(lambda a: expandenv(a, task.env), cc.get('flags', [])))
        flags.extend(cc_flags)

        include_flags = ['-I%(callback_dir)s' % locals()]
        flags.extend(include_flags)

        libcb = task.callback['dylib']
        liblink_flags = ['-L%s' % libcb.parent, '-lcb_%(name)s' % task.callback]
        flags.extend(liblink_flags)

        target = cls.get_default_target(task)
        flags.extend(['-o', str(target)])
        cmd.extend(flags)

        cmd.append(task.srcfile)

        run_in_subproc(cmd, task.env)

        assert target.exists()
        return target

    @classmethod
    def run(cls, target, task):
        cmd = []

        cmd.append(target)

        # first argument: callback param
        cmd.append(task.callback['param'])

        cmd.extend(task.benchmark['args'])

        return run_in_subproc(cmd, task.env)
